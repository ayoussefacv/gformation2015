$(function(){
	var listedUsers=[];
	var listedUsersIds=[];
	$("#listSesssionUsers .element").each(function(index,row) 
	{
		var user = new Object();
		user.id = $(this).attr('element-id');
		listedUsers.push(user);
		listedUsersIds.push(user.id);

		console.log(user.id);
	});
	$("#inputSearchUser").keyup(function() 
	{ 
		var inputSearch = $(this).val();
		var dataString = 'username='+ inputSearch;
		if(inputSearch!='')
		{
			$.ajax({
			type: "POST",
			url: base_url+"/ajax/user/search",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$("#divResultSearchUser").html(html).show();
			}
			});
		}
		return false;    
	});

	jQuery("#divResultSearchUser").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		var decoded = $("<div/>").html($name).text();
		$('#inputSearchUser').val(decoded);
	});

	jQuery(".display_box").live("click",function(e){ 

		var newUser = new Object();

		newUser.id = $(this).attr('element-id');
		newUser.username = $(this).find('.name').text();
		newUser.email = $(this).find('.email').text();
		newUser.status = $(this).find('.status').text();

		console.log("clicked user id ="+newUser.id);
		console.log("clicked user username ="+newUser.username);


		if($.inArray(newUser.id, listedUsersIds)<0)
		{
			// add user to list :
			var dataString = 'sessionId='+ session.id +'&collaboratorId='+ newUser.id;
			if(newUser.id!='')
			{
				$.ajax({
				type: "POST",
				url: base_url+"/ajax/user/addToSession",
				data: dataString,
				cache: false,
				success: function(html)
				{
					$("#listSesssionUsers").append(rowTemplate(newUser));
					console.log(html);
				}
				});
			}
			listedUsersIds.push(newUser.id);

		}
		return false; 

	});
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
		jQuery("#divResultSearchUser").fadeOut(); 
		}
	});
	$('#inputSearchUser').click(function(){
		jQuery("#divResultSearchUser").fadeIn();
	});

	var rowTemplate = function(user)
	{
		var htmlr='<tr style="background:rgba(0,255,0,0.3)">'+
					'<td>'+user.username+'</td>'+
					'<td>'+user.email+'</td>'+
					'<td>'+user.status+'</td>'+
					'<td>'+
						'<a href="'+base_url+'/RF/sessions/'+session.id+'/collaborators/'+user.id+'/delete">delete</a>'+
					'</td>'+
				'</tr>';
		return htmlr;
	}
});