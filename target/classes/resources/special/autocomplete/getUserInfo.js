$(function(){
	var listedUsers=[];
	var listedUsersIds=[];
	$.fn.liveOn = function (event, callback) {
  		$(document).on(event, $(this).selector, callback);  
	}
	$("#listSesssionUsers .element").each(function(index,row) 
	{
		var user = new Object();
		user.id = $(this).attr('element-id');
		listedUsers.push(user);
		listedUsersIds.push(user.id);

		console.log(user.id);
	});
	$("#inputSearchUser").keyup(function() 
	{ 
		var inputSearch = $(this).val();
		var dataString = 'username='+ inputSearch;
		if(inputSearch!='')
		{
			$.ajax({
			type: "POST",
			url: base_url+"/ajax/user/search",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$("#divResultSearchUser").html(html).show();
			}
			});
		}
		return false;    
	});

	$("#divResultSearchUser").liveOn("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		var decoded = $("<div/>").html($name).text();
		$('#inputSearchUser').val(decoded);
	});

	$("#collaboratorsListContainer .display_box").liveOn("click",function(e){ 

		var newUser = new Object();

		newUser.id = $(this).attr('element-id');
		newUser.username = $(this).find('.name').text();
		newUser.email = $(this).find('.email').text();
		newUser.status = $(this).find('.status').text();
		newUser.firstName = $(this).find('.firstName').text();
		newUser.lastName = $(this).find('.lastName').text();
		newUser.confirmed = "not yet";

		console.log("clicked user id ="+newUser.id);
		console.log("clicked user username ="+newUser.username);


		$('#inputSearchUser').val(newUser.username);
		$("#divResultSearchUser").fadeOut(); 

		if($.inArray(newUser.id, listedUsersIds)<0)
		{
			// add user to list :
			var dataString = 'sessionId='+ session.id +'&collaboratorId='+ newUser.id;
			if(newUser.id!='')
			{
				$.ajax({
				type: "POST",
				url: base_url+"/ajax/user/addToSession",
				data: dataString,
				cache: false,
				success: function(html)
				{
					$("#listSesssionUsers").append(rowTemplate(newUser));
					console.log(html);
				}
				});
			}
			listedUsersIds.push(newUser.id);

		}
		return false; 

	});
	$(document).liveOn("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
		$("#divResultSearchUser").fadeOut(); 
		}
	});
	$('#inputSearchUser').click(function(){
		$("#divResultSearchUser").fadeIn();
	});

	var rowTemplate = function(user)
	{
		var htmlr='<tr class="element" style="background:rgba(0,255,0,0.3)">'+
					'<td class="center"><img src="'+base_url+'/resources/assets/images/anonymous.jpg" alt="image" style="width:25px; height:25px;"/></td>'+
					'<td>'+user.lastName+' '+user.firstName+'</td>'+
					'<td>'+user.username+'</td>'+
					'<td>'+user.email+'</td>'+
					'<td>'+user.confirmed+'</td>'+
					'<td class="center">'+
								'<div class="visible-md visible-lg hidden-sm hidden-xs">'+
								'	<button  data-bb="confirm" class="btn btn-red tooltips" data-placement="top" data-original-title="Remove" edit-element-id="'+user.id+'" edit-element-idd="'+session.id+'"><i class="fa fa-times fa fa-white"></i></button>'+
								'</div>'+
					'</td>'+
				'</tr>';
		return htmlr;
	}
});