-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 16 Décembre 2014 à 21:34
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gformation`
--
CREATE DATABASE IF NOT EXISTS `gformation` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gformation`;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nameInDisk` varchar(255) NOT NULL,
  `size` bigint(20) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `session_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`session_id`),
  KEY `FK3737353B982DA89D` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `document`
--

INSERT INTO `document` (`id`, `location`, `name`, `nameInDisk`, `size`, `type`, `session_id`) VALUES
(1, 'E:/tmp/files/', 'SynthÃ¨seFTP (1).docx', 'SynthÃ¨seFTP (1)_1418606581520.docx', 953498, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 1);

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `groups`
--

INSERT INTO `groups` (`id`, `title`, `version`) VALUES
(1, 'groupAdmins', 0),
(2, 'groupManagers', 0),
(3, 'groupTrainers', 0),
(4, 'groupCollaborators', 0);

-- --------------------------------------------------------

--
-- Structure de la table `group_roles`
--

CREATE TABLE IF NOT EXISTS `group_roles` (
  `group_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`group_id`,`role_id`),
  KEY `FK419ECCFD3F1F0A37` (`role_id`),
  KEY `FK419ECCFDD23600BD` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `group_roles`
--

INSERT INTO `group_roles` (`group_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `role`
--

INSERT INTO `role` (`id`, `title`, `version`) VALUES
(1, 'ROLE_ADMIN', 0),
(2, 'ROLE_MANAGER', 0),
(3, 'ROLE_TRAINER', 0),
(4, 'ROLE_COLLABORATOR', 0);

-- --------------------------------------------------------

--
-- Structure de la table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` text,
  `endDate` date DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `trainer_id` bigint(20) DEFAULT NULL,
  `training_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK76508296E5211937` (`training_id`),
  KEY `FK7650829675097CED` (`trainer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `session`
--

INSERT INTO `session` (`id`, `description`, `endDate`, `endTime`, `startDate`, `startTime`, `status`, `title`, `version`, `trainer_id`, `training_id`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', NULL, NULL, NULL, NULL, 'Open', 'session 2', 0, 3, 1),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2015-02-01', '12:00:00', NULL, '09:00:00', 'Open', 'session 1', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `session_collaborators`
--

CREATE TABLE IF NOT EXISTS `session_collaborators` (
  `absent` bit(1) DEFAULT NULL,
  `confirmed` bit(1) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `session_id` bigint(20) NOT NULL DEFAULT '0',
  `collaborator_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`collaborator_id`,`session_id`),
  KEY `FK2A572744CC831BFC` (`collaborator_id`),
  KEY `FK2A572744982DA89D` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `session_collaborators`
--

INSERT INTO `session_collaborators` (`absent`, `confirmed`, `CREATED_DATE`, `session_id`, `collaborator_id`) VALUES
(b'0', b'0', NULL, 1, 1),
(b'0', b'0', NULL, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `training`
--

CREATE TABLE IF NOT EXISTS `training` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` text,
  `endDate` date DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `training`
--

INSERT INTO `training` (`id`, `description`, `endDate`, `startDate`, `status`, `title`, `version`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2015-06-30', '2015-02-01', 'OnHold', 'training 1', 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) DEFAULT NULL,
  `firstName` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK285FEBD23600BD` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `email`, `firstName`, `lastName`, `password`, `status`, `username`, `version`, `group_id`) VALUES
(1, 'admin@admin.com', 'admin1', 'admin1', 'e10adc3949ba59abbe56e057f20f883e', 'ACTIVE', 'admin', 0, 1),
(2, 'manager@manager.com', 'manager1', 'manager1', 'e10adc3949ba59abbe56e057f20f883e', 'ACTIVE', 'manager', 0, 2),
(3, 'trainer@trainer.com', 'trainer1', 'trainer1', 'e10adc3949ba59abbe56e057f20f883e', 'ACTIVE', 'trainer', 0, 3),
(4, 'collaborator@collaborator.com', 'collaborator1', 'collaborator1', 'e10adc3949ba59abbe56e057f20f883e', 'ACTIVE', 'collaborator', 0, 4);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `FK3737353B982DA89D` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`);

--
-- Contraintes pour la table `group_roles`
--
ALTER TABLE `group_roles`
  ADD CONSTRAINT `FK419ECCFDD23600BD` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `FK419ECCFD3F1F0A37` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Contraintes pour la table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `FK7650829675097CED` FOREIGN KEY (`trainer_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK76508296E5211937` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`);

--
-- Contraintes pour la table `session_collaborators`
--
ALTER TABLE `session_collaborators`
  ADD CONSTRAINT `FK2A572744982DA89D` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`),
  ADD CONSTRAINT `FK2A572744CC831BFC` FOREIGN KEY (`collaborator_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK285FEBD23600BD` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
