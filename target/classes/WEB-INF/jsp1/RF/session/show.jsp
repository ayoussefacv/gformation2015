<%@ include file="/WEB-INF/jsp/common/header.jsp" %>


	<h1>Training ${session.training.id }</h1>
	<ul>
		<li>session_id : ${session.id}</li>
		<li>title : ${session.title}</li>
		<li>description : ${session.description}</li>
		<li>startDate : ${session.startDate}</li>
		<li>endDate${session.endDate}</li>
	</ul>
	<!-- =============================================================================================================== session trainer  -->
	<h2>List of sessions</h2>
	<hr /> 
	<hr/>


	<!-- =============================================================================================================== collaborators  -->
	<br/><hr/>
	<div id="collaboratorsListContainer">
		<div class="contentArea">
			<input type="text" class="search" id="inputSearchUser" /> put username<br /> 
			<div id="divResultSearchUser">
			</div>
		</div>
		<table border="1" width="50%"  id="listSesssionUsers">
			<tr>
				<th>username</th>
				<th>email</th>
				<th>status</th>
				<th>options</th>
			</tr>
			
			<div >
				<sw:forEach items="${session.collaborators}" var="o" >
					<tr class="element" element-id="${o.id}">
						<td>${o.username }</td>
						<td>${o.email }</td>
						<td>${o.status }</td>
						<td>
							<a href="${base_url}/RF/sessions/${session.id}/collaborators/${o.id}/delete">delete</a>
						</td>
					</tr>
				</sw:forEach>
			</div>
		</table>
	</div>

	<!-- =============================================================================================================== javascript  -->
	<script type="text/javascript">
			var session=new Object();
				session.id=${session.id};

	</script>

	<script type="text/javascript" src="${resources_url}/special/autocomplete/getUserInfo.js"></script>
	<script type="text/javascript" src="${resources_url}/special/autocomplete/getTrainerInfo.js"></script>
</body>
</html>