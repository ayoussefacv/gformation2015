package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write(" \n");
      out.write("  \n");
      if (_jspx_meth_sw_005fset_005f0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("<!DOCTYPE HTML>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("<title>jQuery File Upload Example</title>\n");
      out.write("<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/js/jquery.1.9.1.min.js\"></script>\n");
      out.write("\n");
      out.write("<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/js/vendor/jquery.ui.widget.js\"></script>\n");
      out.write("<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/js/jquery.iframe-transport.js\"></script>\n");
      out.write("<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/js/jquery.fileupload.js\"></script>\n");
      out.write("\n");
      out.write("<!-- bootstrap just to have good looking page -->\n");
      out.write("<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/bootstrap/js/bootstrap.min.js\"></script>\n");
      out.write("<link href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/bootstrap/css/bootstrap.css\" type=\"text/css\" rel=\"stylesheet\" />\n");
      out.write("\n");
      out.write("<!-- we code these -->\n");
      out.write("<link href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/css/dropzone.css\" type=\"text/css\" rel=\"stylesheet\" />\n");
      out.write("<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/js/myuploadfunction.js\"></script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("<h1>Spring MVC - jQuery File Upload</h1>\n");
      out.write("<div style=\"width:500px;padding:20px\">\n");
      out.write("\n");
      out.write("\t<input id=\"fileupload\" type=\"file\" name=\"files[]\" data-url=\"rest/controller/upload\" multiple>\n");
      out.write("\t\n");
      out.write("\t<div id=\"dropzone\" class=\"fade well\">Drop files here</div>\n");
      out.write("\t\n");
      out.write("\t<div id=\"progress\" class=\"progress\">\n");
      out.write("    \t<div class=\"bar\" style=\"width: 0%;\"></div>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("\t<table id=\"uploaded-files\" class=\"table\">\n");
      out.write("\t\t<tr>\n");
      out.write("\t\t\t<th>File Name</th>\n");
      out.write("\t\t\t<th>File Size</th>\n");
      out.write("\t\t\t<th>File Type</th>\n");
      out.write("\t\t\t<th>Download</th>\n");
      out.write("\t\t</tr>\n");
      out.write("\t</table>\n");
      out.write("\t\n");
      out.write("</div>\n");
      out.write("</body> \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sw_005fset_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f0.setParent(null);
    // /WEB-INF/jsp/index.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/index.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setVar("base_url");
    int _jspx_eval_sw_005fset_005f0 = _jspx_th_sw_005fset_005f0.doStartTag();
    if (_jspx_th_sw_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f1.setParent(null);
    // /WEB-INF/jsp/index.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources/special/fileUpload", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/index.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f1 = _jspx_th_sw_005fset_005f1.doStartTag();
    if (_jspx_th_sw_005fset_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
    return false;
  }
}
