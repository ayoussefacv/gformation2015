package org.apache.jsp.WEB_002dINF.jsp.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class add_005fuser_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(3);
    _jspx_dependants.add("/WEB-INF/jsp/common/header.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/common/sidebar.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/common/footer.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write(" \n");
      out.write("  \n");
      if (_jspx_meth_sw_005fset_005f0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->\n");
      out.write("<!--[if IE 8]><html class=\"ie8\" lang=\"en\"><![endif]-->\n");
      out.write("<!--[if IE 9]><html class=\"ie9\" lang=\"en\"><![endif]-->\n");
      out.write("<!--[if !IE]><!-->\n");
      out.write("<html lang=\"en\">\n");
      out.write("\t<!--<![endif]-->\n");
      out.write("\t<!-- start: HEAD -->\n");
      out.write("\t<head>\n");
      out.write("\t\t<title>Rapido - Responsive Admin Template</title>\n");
      out.write("\t\t<!-- start: META -->\n");
      out.write("\t\t<meta charset=\"utf-8\" />\n");
      out.write("\t\t<!--[if IE]><meta http-equiv='X-UA-Compatible' content=\"IE=edge,IE=9,IE=8,chrome=1\" /><![endif]-->\n");
      out.write("\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\">\n");
      out.write("\t\t<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n");
      out.write("\t\t<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n");
      out.write("\t\t<meta content=\"\" name=\"description\" />\n");
      out.write("\t\t<meta content=\"\" name=\"author\" />\n");
      out.write("\t\t<!-- end: META -->\n");
      out.write("\t\t<!-- start: MAIN CSS -->\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap/css/bootstrap.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/font-awesome/css/font-awesome.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/iCheck/skins/all.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/animate.css/animate.min.css\">\n");
      out.write("\t\t<!-- end: MAIN CSS -->\n");
      out.write("\t\t<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.theme.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.transitions.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/summernote/dist/summernote.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/toastr/toastr.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-select/bootstrap-select.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/css/DT_bootstrap.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css\">\n");
      out.write("\t\t<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->\n");
      out.write("\t\t<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- start: CORE CSS -->\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/styles.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/styles-responsive.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/plugins.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/themes/theme-default.css\" type=\"text/css\" id=\"skin_color\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/print.css\" type=\"text/css\" media=\"print\"/>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getUserInfo.css\">\n");
      out.write("\t\t<!-- end: CORE CSS -->\n");
      out.write("\t\t<link rel=\"shortcut icon\" href=\"favicon.ico\" />\n");
      out.write("\t\t<!-- gformation  -->\n");
      out.write("\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\t\t var base_url = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";\n");
      out.write("\t\t</script>\n");
      out.write("\t</head>\n");
      out.write("\t<!-- end: HEAD -->\n");
      out.write("\t<!-- start: BODY -->\n");
      out.write("\t<body>\n");
      out.write("\t\t<!-- start: SLIDING BAR (SB) -->\n");
      out.write("\t\t<!-- end: SLIDING BAR -->\n");
      out.write("\t\t<div class=\"main-wrapper\">\n");
      out.write("\t\t\t<!-- start: TOPBAR -->\n");
      out.write("\t\t\t<header class=\"topbar navbar navbar-inverse navbar-fixed-top inner\">\n");
      out.write("\t\t\t\t<!-- start: TOPBAR CONTAINER -->\n");
      out.write("\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t<div class=\"navbar-header\">\n");
      out.write("\t\t\t\t\t\t<a class=\"sb-toggle-left hidden-md hidden-lg\" href=\"#main-navbar\">\n");
      out.write("\t\t\t\t\t\t\t<i class=\"fa fa-bars\"></i>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t<!-- start: LOGO -->\n");
      out.write("\t\t\t\t\t\t<a class=\"navbar-brand\" href=\"index.html\">\n");
      out.write("\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/logo.png\" alt=\"Rapido\"/>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t<!-- end: LOGO -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"topbar-tools\">\n");
      out.write("\t\t\t\t\t\t<!-- start: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"nav navbar-right\">\n");
      out.write("\t\t\t\t\t\t\t<!-- start: USER DROPDOWN -->\n");
      out.write("\t\t\t\t\t\t\t<li class=\"dropdown current-user\">\n");
      out.write("\t\t\t\t\t\t\t\t<a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" data-close-others=\"true\" href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/avatar-1-small.jpg\" class=\"img-circle\" alt=\"\"> <span class=\"username hidden-xs\">Peter Clark</span> <i class=\"fa fa-caret-down \"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-dark\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"login_login.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tLog Out\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t<!-- end: USER DROPDOWN -->\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t<!-- end: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end: TOPBAR CONTAINER -->\n");
      out.write("\t\t\t</header>");
      out.write('\n');
      out.write("\n");
      out.write(" \n");
      out.write("  \n");
      if (_jspx_meth_sw_005fset_005f2(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t<!-- start: PAGESLIDE LEFT -->\n");
      out.write("\t\t\t<a class=\"closedbar inner hidden-sm hidden-xs\" href=\"#\">\n");
      out.write("\t\t\t</a>\n");
      out.write("\t\t\t<nav id=\"pageslide-left\" class=\"pageslide inner\">\n");
      out.write("\t\t\t\t<div class=\"navbar-content\">\n");
      out.write("\t\t\t\t\t<!-- start: SIDEBAR -->\n");
      out.write("\t\t\t\t\t<div class=\"main-navigation left-wrapper transition-left\">\n");
      out.write("\t\t\t\t\t\t<div class=\"navigation-toggler hidden-sm hidden-xs\">\n");
      out.write("\t\t\t\t\t\t\t<a href=\"#main-navbar\" class=\"sb-toggle-left\">\n");
      out.write("\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"user-profile border-top padding-horizontal-10 block\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"inline-block\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/avatar-1.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"inline-block\">\n");
      out.write("\t\t\t\t\t\t\t\t<h5 class=\"no-margin\"> Welcome </h5>\n");
      out.write("\t\t\t\t\t\t\t\t<h4 class=\"no-margin\"> Peter Clark </h4>\n");
      out.write("\t\t\t\t\t\t\t\t<a class=\"btn user-options sb_toggle\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"fa fa-cog\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- start: MAIN NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"main-navigation-menu\">\n");
      out.write("\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"index.html\"><i class=\"fa fa-home\"></i> <span class=\"title\"> Dashboard </span><span class=\"label label-default pull-right \">LABEL</span> </a>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-desktop\"></i> <span class=\"title\"> Layouts </span><i class=\"icon-arrow\"></i> </a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tHorizontal Menu <i class=\"icon-arrow\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_menu.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tHorizontal Menu\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_menu_fixed.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tHorizontal Menu Fixed\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_sidebar_menu.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tHorizontal &amp; Sidebar Menu\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_sidebar_closed.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\"> Sidebar Closed </span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_sidebar_not_fixed.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\"> Sidebar Not Fixed </span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_boxed_layout.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\"> Boxed Layout </span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_footer_fixed.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\"> Footer Fixed </span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_single_page.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\"> Single-Page Interface </span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t<li class=\"active open\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-code\"></i> <span class=\"title\">Pages</span><i class=\"icon-arrow\"></i> </a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_user_profile.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">User Profile</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_invoice.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Invoice</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_gallery.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Gallery</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_timeline.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Timeline</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_calendar.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Calendar</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_messages.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Messages</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li class=\"active\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_blank_page.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Blank Page</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t<!-- end: MAIN NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- end: SIDEBAR -->\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"slide-tools\">\n");
      out.write("\t\t\t\t\t<div class=\"col-xs-6 text-left no-padding\">\n");
      out.write("\t\t\t\t\t\t<a class=\"btn btn-sm status\" href=\"#\">\n");
      out.write("\t\t\t\t\t\t\tStatus <i class=\"fa fa-dot-circle-o text-green\"></i> <span>Online</span>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"col-xs-6 text-right no-padding\">\n");
      out.write("\t\t\t\t\t\t<a class=\"btn btn-sm log-out text-right\" href=\"login_login.html\">\n");
      out.write("\t\t\t\t\t\t\t<i class=\"fa fa-power-off\"></i> Log Out\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</nav>\n");
      out.write("\t\t\t<!-- end: PAGESLIDE LEFT -->");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- start: MAIN CONTAINER -->\n");
      out.write("\t\t\t<div class=\"main-container inner\">\n");
      out.write("\t\t\t\t<!-- start: PAGE -->\n");
      out.write("\t\t\t\t<div class=\"main-content\">\n");
      out.write("\t\t\t\t\t<!-- start: PANEL CONFIGURATION MODAL FORM -->\n");
      out.write("\t\t\t\t\t<div class=\"modal fade\" id=\"panel-config\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n");
      out.write("\t\t\t\t\t\t<div class=\"modal-dialog\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"modal-content\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t&times;\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h4 class=\"modal-title\">Panel Configuration</h4>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\tHere will be a configuration form\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-footer\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tClose\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tSave changes\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<!-- /.modal-content -->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- /.modal-dialog -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- /.modal -->\n");
      out.write("\t\t\t\t\t<!-- end: SPANEL CONFIGURATION MODAL FORM -->\n");
      out.write("\t\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t\t<!-- start: PAGE HEADER -->\n");
      out.write("\t\t\t\t\t\t<!-- start: TOOLBAR -->\n");
      out.write("\t\t\t\t\t\t<div class=\"toolbar row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-sm-6 hidden-xs\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"page-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h1>Blank Page <small>subtitle here</small></h1>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"back-subviews\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"fa fa-chevron-left\"></i> BACK\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"close-subviews\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i> CLOSE\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"toolbar-tools pull-right\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<!-- start: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t\t\t\t<ul class=\"nav navbar-right\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<!-- start: TO-DO DROPDOWN -->\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" data-close-others=\"true\" href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-plus\"></i> SUBVIEW\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tooltip-notification hide\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tooltip-notification-arrow\"></div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-light dropdown-subview\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tNotes\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#newNote\" class=\"new-note\"><span class=\"fa-stack\"> <i class=\"fa fa-file-text-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-plus fa-stack-1x stack-right-bottom text-danger\"></i> </span> Add new note</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#readNote\" class=\"read-all-notes\"><span class=\"fa-stack\"> <i class=\"fa fa-file-text-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-share fa-stack-1x stack-right-bottom text-danger\"></i> </span> Read all notes</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tCalendar\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#newEvent\" class=\"new-event\"><span class=\"fa-stack\"> <i class=\"fa fa-calendar-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-plus fa-stack-1x stack-right-bottom text-danger\"></i> </span> Add new event</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#showCalendar\" class=\"show-calendar\"><span class=\"fa-stack\"> <i class=\"fa fa-calendar-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-share fa-stack-1x stack-right-bottom text-danger\"></i> </span> Show calendar</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tContributors\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#newContributor\" class=\"new-contributor\"><span class=\"fa-stack\"> <i class=\"fa fa-user fa-stack-1x fa-lg\"></i> <i class=\"fa fa-plus fa-stack-1x stack-right-bottom text-danger\"></i> </span> Add new contributor</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#showContributors\" class=\"show-contributors\"><span class=\"fa-stack\"> <i class=\"fa fa-user fa-stack-1x fa-lg\"></i> <i class=\"fa fa-share fa-stack-1x stack-right-bottom text-danger\"></i> </span> Show all contributor</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t<!-- end: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- end: TOOLBAR -->\n");
      out.write("\t\t\t\t\t\t<!-- end: PAGE HEADER -->\n");
      out.write("\t\t\t\t\t\t<!-- start: BREADCRUMB -->\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<ol class=\"breadcrumb\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tDashboard\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li class=\"active\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tBlank Page\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ol>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- end: BREADCRUMB -->\n");
      out.write("\t\t\t\t\t\t<!-- start: PAGE CONTENT -->\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"panel panel-white\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div >\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<h3>Add new contributor</h3>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<form method=\"POST\" action=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/admin/users_manager\" class=\"form-contributor\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"errorHandler alert alert-danger no-display\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times-sign\"></i> You have some form errors. Please check below.\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"successHandler alert alert-success no-display\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-ok\"></i> Your form validation is successful!\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"contributor-id hide\" type=\"text\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFirst Name  <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Insert your First Name\" class=\"form-control contributor-firstname\" name=\"firstname\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLast Name <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Insert your Last Name\" class=\"form-control contributor-lastname\" name=\"lastname\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tEmail Address <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"email\" placeholder=\"Text Field\" class=\"form-control contributor-email\" name=\"email\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tPassword <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control contributor-password\" name=\"password\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tConfirm Password <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control contributor-password-again\" name=\"password_again\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tGender <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"radio-inline\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"grey contributor-gender\" value=\"F\" name=\"gender\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFemale\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"radio-inline\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"grey contributor-gender\" value=\"M\" name=\"gender\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMale\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tPermits <span class=\"symbol required\"></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select name=\"permits\" class=\"form-control contributor-permits\" >\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"View and Edit\">View and Edit</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"View Only\">View Only</option>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"pull-right\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-info save-contributor\" type=\"submit\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tEnregister\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</form>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<!-- end: PAGE CONTENT-->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"subviews\">\n");
      out.write("\t\t\t\t\t\t<div class=\"subviews-container\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end: PAGE -->\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- end: MAIN CONTAINER -->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \n");
      out.write("  \n");
      if (_jspx_meth_sw_005fset_005f4(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f5(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<!-- end: PAGE CONTENT-->\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t<div class=\"subviews\">\n");
      out.write("\t\t\t\t\t\t<div class=\"subviews-container\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end: PAGE -->\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- end: MAIN CONTAINER -->\n");
      out.write("\t\t\t<!-- start: FOOTER -->\n");
      out.write("\t\t\t<footer class=\"inner\">\n");
      out.write("\t\t\t\t<div class=\"footer-inner\">\n");
      out.write("\t\t\t\t\t<div class=\"pull-left\">\n");
      out.write("\t\t\t\t\t\t2014 &copy; Rapido by cliptheme.\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"pull-right\">\n");
      out.write("\t\t\t\t\t\t<span class=\"go-top\"><i class=\"fa fa-chevron-up\"></i></span>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</footer>\n");
      out.write("\t\t\t<!-- end: FOOTER -->\n");
      out.write("\t\t\t<!-- start: SUBVIEW SAMPLE CONTENTS -->\n");
      out.write("\t\t\t<!-- *** NEW NOTE *** -->\n");
      out.write("\t\t\t<div id=\"newNote\">\n");
      out.write("\t\t\t\t<div class=\"noteWrap col-md-8 col-md-offset-2\">\n");
      out.write("\t\t\t\t\t<h3>Add new note</h3>\n");
      out.write("\t\t\t\t\t<form class=\"form-note\">\n");
      out.write("\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t<input class=\"note-title form-control\" name=\"noteTitle\" type=\"text\" placeholder=\"Note Title...\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t<textarea id=\"noteEditor\" name=\"noteEditor\" class=\"hide\"></textarea>\n");
      out.write("\t\t\t\t\t\t\t<textarea class=\"summernote\" placeholder=\"Write note here...\"></textarea>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"pull-right\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"btn-group\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-info close-subview-button\">\n");
      out.write("\t\t\t\t\t\t\t\t\tClose\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"btn-group\">\n");
      out.write("\t\t\t\t\t\t\t\t<button class=\"btn btn-info save-note\" type=\"submit\">\n");
      out.write("\t\t\t\t\t\t\t\t\tSave\n");
      out.write("\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</form>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- *** READ NOTE *** -->\n");
      out.write("\n");
      out.write("\t\t\t<!-- end: SUBVIEW SAMPLE CONTENTS -->\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<!-- start: MAIN JAVASCRIPTS -->\n");
      out.write("\t\t<!--[if lt IE 9]>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/respond.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/excanvas.min.js\"></script>\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jQuery/jquery-1.11.1.min.js\"></script>\n");
      out.write("\t\t<![endif]-->\n");
      out.write("\t\t<!--[if gte IE 9]><!-->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jQuery/jquery-2.1.1.min.js\"></script>\n");
      out.write("\t\t<!--<![endif]-->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap/js/bootstrap.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/blockUI/jquery.blockUI.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/iCheck/jquery.icheck.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/moment/min/moment.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootbox/bootbox.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery.appear/jquery.appear.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-cookie/jquery.cookie.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/velocity/jquery.velocity.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/TouchSwipe/jquery.touchSwipe.min.js\"></script>\n");
      out.write("\t\t<!-- end: MAIN JAVASCRIPTS -->\n");
      out.write("\t\t<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-mockjax/jquery.mockjax.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/toastr/toastr.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modal.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-select/bootstrap-select.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-validation/dist/jquery.validate.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/js/jquery.dataTables.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/js/DT_bootstrap.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/truncate/jquery.truncate.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/summernote/dist/summernote.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-daterangepicker/daterangepicker.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/subview.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/subview-examples.js\"></script>\n");
      out.write("\t\t<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\t\t<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- start: CORE JAVASCRIPTS  -->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/main.js\"></script>\n");
      out.write("\t\t<!-- end: CORE JAVASCRIPTS  -->\n");
      out.write("\t\t<script>\n");
      out.write("\t\t\tjQuery(document).ready(function() {\n");
      out.write("\t\t\t\tMain.init();\n");
      out.write("\t\t\t\tSVExamples.init();\n");
      out.write("\t\t\t});\n");
      out.write("\t\t</script>\n");
      out.write("\t</body>\n");
      out.write("\t<!-- end: BODY -->\n");
      out.write("</html>");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sw_005fset_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f0.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setVar("base_url");
    int _jspx_eval_sw_005fset_005f0 = _jspx_th_sw_005fset_005f0.doStartTag();
    if (_jspx_th_sw_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f1.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f1 = _jspx_th_sw_005fset_005f1.doStartTag();
    if (_jspx_th_sw_005fset_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f2.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f2.setParent(null);
    // /WEB-INF/jsp/common/sidebar.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f2.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/sidebar.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f2.setVar("base_url");
    int _jspx_eval_sw_005fset_005f2 = _jspx_th_sw_005fset_005f2.doStartTag();
    if (_jspx_th_sw_005fset_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f2);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f3 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f3.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f3.setParent(null);
    // /WEB-INF/jsp/common/sidebar.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f3.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/sidebar.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f3.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f3 = _jspx_th_sw_005fset_005f3.doStartTag();
    if (_jspx_th_sw_005fset_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f3);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f4 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f4.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f4.setParent(null);
    // /WEB-INF/jsp/common/footer.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f4.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/footer.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f4.setVar("base_url");
    int _jspx_eval_sw_005fset_005f4 = _jspx_th_sw_005fset_005f4.doStartTag();
    if (_jspx_th_sw_005fset_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f4);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f4);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f5(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f5 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f5.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f5.setParent(null);
    // /WEB-INF/jsp/common/footer.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f5.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/footer.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f5.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f5 = _jspx_th_sw_005fset_005f5.doStartTag();
    if (_jspx_th_sw_005fset_005f5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f5);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f5);
    return false;
  }
}
