package org.apache.jsp.WEB_002dINF.jsp.session;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class show_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/WEB-INF/jsp/common/header.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write(" \r\n");
      out.write("  \r\n");
      if (_jspx_meth_sw_005fset_005f0(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("\t<title>Insert title here</title>\r\n");
      out.write("\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/lib/jquery-1.8.0.min.js\"></script>\r\n");
      out.write("\t<script type=\"text/javascript\"> var base_url = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";</script>\r\n");
      out.write("\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getUserInfo.js\"></script>\r\n");
      out.write("\t<link href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getUserInfo.css\" rel=\"stylesheet\" />\r\n");
      out.write("</head>\r\n");
      out.write("<body>");
      out.write("\n");
      out.write("\n");
      out.write("\t<h1>Training ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.training.id }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</h1>\n");
      out.write("\t<ul>\n");
      out.write("\t\t<li>session_id : ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</li>\n");
      out.write("\t\t<li>title : ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.title}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</li>\n");
      out.write("\t\t<li>description : ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.description}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</li>\n");
      out.write("\t\t<li>startDate : ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.startDate}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</li>\n");
      out.write("\t\t<li>endDate");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.endDate}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</li>\n");
      out.write("\t</ul>\n");
      out.write("\t<h2>List of sessions</h2>\n");
      out.write("\t<hr />\n");
      out.write("\t<a href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/RF/sessions/");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/update\">update session</a>\n");
      out.write("\t<table border=\"1\" width=\"50%\"  id=\"listSesssionUsers\">\n");
      out.write("\t\t<tr>\n");
      out.write("\t\t\t<div class=\"contentArea\">\n");
      out.write("\t\t\t\t<input type=\"text\" class=\"search\" id=\"inputSearchUser\" /> put username<br /> \n");
      out.write("\t\t\t\t<div id=\"divResultSearchUser\">\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</tr>\n");
      out.write("\t\t<tr>\n");
      out.write("\t\t\t<th>username</th>\n");
      out.write("\t\t\t<th>email</th>\n");
      out.write("\t\t\t<th>status</th>\n");
      out.write("\t\t\t<th>options</th>\n");
      out.write("\t\t</tr>\n");
      out.write("\t\t\n");
      out.write("\t\t<div >\n");
      out.write("\t\t\t");
      if (_jspx_meth_sw_005fforEach_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t</div>\n");
      out.write("\t</table>\n");
      out.write("\t<table border=\"1\" width=\"50%\"  id=\"listSesssionUsers\">\n");
      out.write("\t\t<tr>\n");
      out.write("\t\t\t<div class=\"upload\">\n");
      out.write("\t\t\t\t<input type=\"text\" class=\"search\" id=\"inputSearchUser\" /> upload file<br /> \n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</tr>\n");
      out.write("\t\t<tr>\n");
      out.write("\t\t\t<th>username</th>\n");
      out.write("\t\t\t<th>email</th>\n");
      out.write("\t\t\t<th>status</th>\n");
      out.write("\t\t\t<th>options</th>\n");
      out.write("\t\t</tr>\n");
      out.write("\t\t\n");
      out.write("\t\t<div >\n");
      out.write("\t\t\t");
      if (_jspx_meth_sw_005fforEach_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t</div>\n");
      out.write("\t</table>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t<script type=\"text/javascript\">\n");
      out.write("\t\t\tvar session=new Object();\n");
      out.write("\t\t\t\tsession.id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(";\n");
      out.write("\n");
      out.write("\t</script>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sw_005fset_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f0.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setVar("base_url");
    int _jspx_eval_sw_005fset_005f0 = _jspx_th_sw_005fset_005f0.doStartTag();
    if (_jspx_th_sw_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f1.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f1 = _jspx_th_sw_005fset_005f1.doStartTag();
    if (_jspx_th_sw_005fset_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fforEach_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_sw_005fforEach_005f0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_sw_005fforEach_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fforEach_005f0.setParent(null);
    // /WEB-INF/jsp/session/show.jsp(30,3) name = items type = java.lang.Object reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.collaborators}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/session/show.jsp(30,3) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f0.setVar("o");
    int[] _jspx_push_body_count_sw_005fforEach_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_sw_005fforEach_005f0 = _jspx_th_sw_005fforEach_005f0.doStartTag();
      if (_jspx_eval_sw_005fforEach_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t\t\t<tr class=\"element\" element-id=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\">\n");
          out.write("\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.username }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.email }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.status }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t<td>\n");
          out.write("\t\t\t\t\t\t<a href=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/RF/sessions/");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/collaborators/");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/delete\">delete</a>\n");
          out.write("\t\t\t\t\t</td>\n");
          out.write("\t\t\t\t</tr>\n");
          out.write("\t\t\t");
          int evalDoAfterBody = _jspx_th_sw_005fforEach_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_sw_005fforEach_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_sw_005fforEach_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_sw_005fforEach_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_sw_005fforEach_005f0.doFinally();
      _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_sw_005fforEach_005f0);
    }
    return false;
  }

  private boolean _jspx_meth_sw_005fforEach_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_sw_005fforEach_005f1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_sw_005fforEach_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fforEach_005f1.setParent(null);
    // /WEB-INF/jsp/session/show.jsp(56,3) name = items type = java.lang.Object reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.documents}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/session/show.jsp(56,3) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f1.setVar("o");
    int[] _jspx_push_body_count_sw_005fforEach_005f1 = new int[] { 0 };
    try {
      int _jspx_eval_sw_005fforEach_005f1 = _jspx_th_sw_005fforEach_005f1.doStartTag();
      if (_jspx_eval_sw_005fforEach_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t\t\t<tr class=\"element\" element-id=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\">\n");
          out.write("\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.name }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.url }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t<td>\n");
          out.write("\t\t\t\t\t\t<a href=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/documents/");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\">download</a>\n");
          out.write("\t\t\t\t\t\t<a href=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/documents/");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/delete\">delete</a>\n");
          out.write("\t\t\t\t\t</td>\n");
          out.write("\t\t\t\t</tr>\n");
          out.write("\t\t\t");
          int evalDoAfterBody = _jspx_th_sw_005fforEach_005f1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_sw_005fforEach_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_sw_005fforEach_005f1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_sw_005fforEach_005f1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_sw_005fforEach_005f1.doFinally();
      _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_sw_005fforEach_005f1);
    }
    return false;
  }
}
