package org.apache.jsp.WEB_002dINF.jsp.RF.session;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class show_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(6);
    _jspx_dependants.add("/WEB-INF/jsp/common/header.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/common/sidebar.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/modules/addTrainerToSession.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/modules/addCollaboratorsToSession.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/modules/session/update.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/common/footer_2.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fchoose;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fotherwise;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fchoose = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fotherwise = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.release();
    _005fjspx_005ftagPool_005fsw_005fchoose.release();
    _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.release();
    _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.release();
    _005fjspx_005ftagPool_005fsw_005fotherwise.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write(" \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 if (request.getAttribute("error") != null) { 
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t     \t\t\t ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${error}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write('\n');
 } 
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->\n");
      out.write("<!--[if IE 8]><html class=\"ie8\" lang=\"en\"><![endif]-->\n");
      out.write("<!--[if IE 9]><html class=\"ie9\" lang=\"en\"><![endif]-->\n");
      out.write("<!--[if !IE]><!-->\n");
      out.write("<html lang=\"en\">\n");
      out.write("\t<!--<![endif]-->\n");
      out.write("\t<!-- start: HEAD -->\n");
      out.write("\t<head>\n");
      out.write("\t\t<title>G-Formation</title>\n");
      out.write("\t\t<!-- start: META -->\n");
      out.write("\t\t<meta charset=\"utf-8\" />\n");
      out.write("\t\t<!--[if IE]><meta http-equiv='X-UA-Compatible' content=\"IE=edge,IE=9,IE=8,chrome=1\" /><![endif]-->\n");
      out.write("\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\">\n");
      out.write("\t\t<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n");
      out.write("\t\t<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n");
      out.write("\t\t<meta content=\"\" name=\"description\" />\n");
      out.write("\t\t<meta content=\"\" name=\"author\" />\n");
      out.write("\t\t<!-- end: META -->\n");
      out.write("\t\t<!-- start: MAIN CSS -->\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap/css/bootstrap.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/font-awesome/css/font-awesome.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/iCheck/skins/all.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/animate.css/animate.min.css\">\n");
      out.write("\t\t<!-- end: MAIN CSS -->\n");
      out.write("\t\t<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.theme.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.transitions.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/summernote/dist/summernote.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/toastr/toastr.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-select/bootstrap-select.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/css/DT_bootstrap.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css\">\n");
      out.write("\t\t<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->\n");
      out.write("\t\t<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- start: CORE CSS -->\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/styles.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/styles-responsive.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/plugins.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/themes/theme-default.css\" type=\"text/css\" id=\"skin_color\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/print.css\" type=\"text/css\" media=\"print\"/>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getUserInfo.css\">\n");
      out.write("\t\t<!-- end: CORE CSS -->\n");
      out.write("\t\t<link rel=\"shortcut icon\" href=\"favicon.ico\" />\n");
      out.write("\t\t<!-- gformation  -->\n");
      out.write("\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\t\t var base_url = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";\t\n");
      out.write("\t\t\t\t var session = new Object();\n");
      out.write("\t\t \t\t\tsession.id = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";\n");
      out.write("</script>\n");
      out.write("\t</head>\n");
      out.write("\t<!-- end: HEAD -->\n");
      out.write("\t<!-- start: BODY -->\n");
      out.write("\t<body>\n");
      out.write("\t\t<!-- start: SLIDING BAR (SB) -->\n");
      out.write("\t\t<!-- end: SLIDING BAR -->\n");
      out.write("\t\t<div class=\"main-wrapper\">\n");
      out.write("\t\t\t<!-- start: TOPBAR -->\n");
      out.write("\t\t\t<header class=\"topbar navbar navbar-inverse navbar-fixed-top inner\">\n");
      out.write("\t\t\t\t<!-- start: TOPBAR CONTAINER -->\n");
      out.write("\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t<div class=\"navbar-header\">\n");
      out.write("\t\t\t\t\t\t<a class=\"sb-toggle-left hidden-md hidden-lg\" href=\"#main-navbar\">\n");
      out.write("\t\t\t\t\t\t\t<i class=\"fa fa-bars\"></i>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t<!-- start: LOGO -->\n");
      out.write("\t\t\t\t\t\t<a class=\"navbar-brand\" href=\"index.html\">\n");
      out.write("\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/logo.png\" alt=\"Rapido\"/>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t<!-- end: LOGO -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"topbar-tools\">\n");
      out.write("\t\t\t\t\t\t<!-- start: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"nav navbar-right\">\n");
      out.write("\t\t\t\t\t\t\t<!-- start: USER DROPDOWN -->\n");
      out.write("\t\t\t\t\t\t\t<li class=\"dropdown current-user\">\n");
      out.write("\t\t\t\t\t\t\t\t<a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" data-close-others=\"true\" href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/anonymous.jpg\" width=\"25\" class=\"img-circle\" alt=\"\"> <span class=\"username hidden-xs\">Mr. Admin</span> \n");
      out.write("\t\t\t\t\t\t\t\t\t <i class=\"fa fa-caret-down \"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-dark\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/login?logout\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tLog Out\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t<!-- end: USER DROPDOWN -->\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t<!-- end: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end: TOPBAR CONTAINER -->\n");
      out.write("\t\t\t</header>");
      out.write('\n');
      out.write("\n");
      out.write(" \n");
      out.write("  \n");
      if (_jspx_meth_sw_005fset_005f2(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t<!-- start: PAGESLIDE LEFT -->\n");
      out.write("\t\t\t<a class=\"closedbar inner hidden-sm hidden-xs\" href=\"#\">\n");
      out.write("\t\t\t</a>\n");
      out.write("\t\t\t<nav id=\"pageslide-left\" class=\"pageslide inner\">\n");
      out.write("\t\t\t\t<div class=\"navbar-content\">\n");
      out.write("\t\t\t\t\t<!-- start: SIDEBAR -->\n");
      out.write("\t\t\t\t\t<div class=\"main-navigation left-wrapper transition-left\">\n");
      out.write("\t\t\t\t\t\t<div class=\"navigation-toggler hidden-sm hidden-xs\">\n");
      out.write("\t\t\t\t\t\t\t<a href=\"#main-navbar\" class=\"sb-toggle-left\">\n");
      out.write("\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"user-profile border-top padding-horizontal-10 block\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"inline-block\">\n");
      out.write("\t\t\t\t\t\t\t\t<img width=\"45px\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/anonymous.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"inline-block\">\n");
      out.write("\t\t\t\t\t\t\t\t<h5 class=\"no-margin\"> Bienvenu </h5>\n");
      out.write("\t\t\t\t\t\t\t\t<h4 class=\"no-margin\"> Mr. Admin </h4>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- start: MAIN NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"main-navigation-menu\">\n");
      out.write("\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"index.html\"><i class=\"fa fa-home\"></i> <span class=\"title\"> Acceuil </span><span class=\"label label-default pull-right \">Home</span> </a>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-desktop\"></i> <span class=\"title\"> Administration </span><i class=\"icon-arrow\"></i> </a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tUtilisateurs <i class=\"icon-arrow\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_menu.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tAjouter\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tGroups <i class=\"icon-arrow\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_menu.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tAjouter\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t<li class=\"active open\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-code\"></i> <span class=\"title\">Formations</span><i class=\"icon-arrow\"></i> </a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_user_profile.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Liste Formations</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t<!-- end: MAIN NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- end: SIDEBAR -->\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"slide-tools\">\n");
      out.write("\t\t\t\t\t<div class=\"col-xs-6 text-left no-padding\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"col-xs-6 text-right no-padding\">\n");
      out.write("\t\t\t\t\t\t<a class=\"btn btn-sm log-out text-right\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/login?logout\">\n");
      out.write("\t\t\t\t\t\t\t<i class=\"fa fa-power-off\"></i> Log Out\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</nav>\n");
      out.write("\t\t\t<!-- end: PAGESLIDE LEFT -->");
      out.write("\n");
      out.write("<!-- ============================================================================================ -->\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("\t\t var base_url = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";\n");
      out.write("\t\t var session = new Object();\n");
      out.write("\t\t session.id = ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(";\n");
      out.write("</script>\n");
      out.write("<!-- ============================================================================================ -->\n");
      out.write("\n");
      out.write("\t\t\t<!-- start: PAGESLIDE RIGHT -->\n");
      out.write("\t\t\t<!-- end: PAGESLIDE RIGHT -->\n");
      out.write("\t\t\t<!-- start: MAIN CONTAINER -->\n");
      out.write("\t\t\t<div class=\"main-container inner\">\n");
      out.write("\t\t\t\t<!-- start: PAGE -->\n");
      out.write("\t\t\t\t<div class=\"main-content\">\n");
      out.write("\t\t\t\t\t<!-- start: PANEL CONFIGURATION MODAL FORM -->\n");
      out.write("\t\t\t\t\t<div class=\"modal fade\" id=\"panel-config\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n");
      out.write("\t\t\t\t\t\t<div class=\"modal-dialog\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"modal-content\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t&times;\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h4 class=\"modal-title\">Panel Configuration</h4>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\tHere will be a configuration form\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-footer\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tClose\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tSave changes\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<!-- /.modal-content -->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- /.modal-dialog -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- /.modal -->\n");
      out.write("\t\t\t\t\t<!-- end: SPANEL CONFIGURATION MODAL FORM -->\n");
      out.write("\t\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t\t<!-- start: PAGE HEADER -->\n");
      out.write("\t\t\t\t\t\t<!-- start: TOOLBAR -->\n");
      out.write("\t\t\t\t\t\t<div class=\"toolbar row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-sm-6 hidden-xs\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"page-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h2>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.title}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</h2>\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/RF/sessions/");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/update\">update session</a>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- end: TOOLBAR -->\n");
      out.write("\t\t\t\t\t\t<!-- end: PAGE HEADER -->\n");
      out.write("\t\t\t\t\t\t<br/>\n");
      out.write("\t\t\t\t\t\t<!-- start: PAGE CONTENT -->\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"tabbable\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-tabs tab-padding tab-space-3 tab-blue\" id=\"myTab4\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li class=\"active\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a data-toggle=\"tab\" href=\"#panel_overview\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\toverview\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a data-toggle=\"tab\" href=\"#panel_trainer\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\tTrainer\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a data-toggle=\"tab\" href=\"#panel_collaborators\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\tcollaborators\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a data-toggle=\"tab\" href=\"#panel_update\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\tupdate\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"tab-content\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div id=\"panel_overview\" class=\"tab-pane fade in active\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-5 col-md-4\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-left\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-condensed table-hover\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"3\">details</th>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>title</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.title}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><a href=\"#panel_update\" class=\"show-tab\"><i class=\"fa fa-pencil edit-user-info\"></i></a></td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>start date</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.startDate}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><a href=\"#panel_update\" class=\"show-tab\"><i class=\"fa fa-pencil edit-user-info\"></i></a></td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>start time</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.startTime}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><a href=\"#panel_update\" class=\"show-tab\"><i class=\"fa fa-pencil edit-user-info\"></i></a></td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>end time</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.endTime}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><a href=\"#panel_update\" class=\"show-tab\"><i class=\"fa fa-pencil edit-user-info\"></i></a></td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>status\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
      if (_jspx_meth_sw_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
      if (_jspx_meth_sw_005fif_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><button id=\"sessionStatus\" class=\"btn ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${btnColor}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\" sessionStatus=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.status}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write('"');
      out.write('>');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.status}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button></td>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-7 col-md-8\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>description</b>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.description}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div id=\"panel_trainer\" class=\"tab-pane fade\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t...\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
      out.write("\t<div class=\"row\" id=\"getTrainerContainer\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"contentArea col-sm-5 col-md-4\">\r\n");
      out.write("\t\t\t<div class=\"col-sm-12\">\r\n");
      out.write("\t\t\t\t<span class=\"input-icon\">\r\n");
      out.write("\t\t\t\t\t<input  type=\"text\"  id=\"inputSearchTrainer\" placeholder=\"put username\" id=\"form-field-14\" class=\"form-control search\">\r\n");
      out.write("\t\t\t\t\t<i class=\"fa fa-user\"></i> </span>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t\t<div class=\"col-sm-12\" id=\"divResultSearchTrainer\">\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"contentArea col-sm-7 col-md-8\">\r\n");
      out.write("\t\t\t<table border=\"1\" width=\"50%\"  id=\"listSesssionTrainer\">\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<th>username</th>\r\n");
      out.write("\t\t\t\t\t<th>email</th>\r\n");
      out.write("\t\t\t\t\t<th>status</th>\r\n");
      out.write("\t\t\t\t\t<th>options</th>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t");
      if (_jspx_meth_sw_005fchoose_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t\t\t</table>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div id=\"panel_collaborators\" class=\"tab-pane fade\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t...\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
      out.write("\t<div class=\"row\" id=\"collaboratorsListContainer\">\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"contentArea col-sm-5 col-md-4\">\r\n");
      out.write("\t\t\t<div class=\"col-sm-12\">\r\n");
      out.write("\t\t\t\t<span class=\"input-icon\">\r\n");
      out.write("\t\t\t\t\t<input  type=\"text\"  id=\"inputSearchUser\" placeholder=\"put username\" id=\"form-field-14\" class=\"form-control search\">\r\n");
      out.write("\t\t\t\t\t<i class=\"fa fa-user\"></i> </span>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<div id=\"divResultSearchUser\" class=\"col-sm-12\">\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"contentArea col-sm-7 col-md-8\">\r\n");
      out.write("\t\t\t<table class=\"table table-striped table-bordered table-hover\" id=\"listSesssionUsers\">\r\n");
      out.write("\t\t\t\t<thead>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<th class=\"center\"></th>\r\n");
      out.write("\t\t\t\t\t<th>Full Name</th>\r\n");
      out.write("\t\t\t\t\t<th class=\"hidden-xs\">Username</th>\r\n");
      out.write("\t\t\t\t\t<th class=\"hidden-xs\">Email</th>\r\n");
      out.write("\t\t\t\t\t<th class=\"hidden-xs\">confirmation</th>\r\n");
      out.write("\t\t\t\t\t<th></th>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t</thead>\r\n");
      out.write("\t\t\t\t<tbody>\r\n");
      out.write("\t\t\t\t\t");
      if (_jspx_meth_sw_005fforEach_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t\t\t\t</tbody>\r\n");
      out.write("\t\t\t</table>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<!-- ====================================================================================================== -->\r\n");
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div id=\"panel_update\" class=\"tab-pane fade\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t...\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<form method=\"POST\" action=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/RF/sessions/");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/update\" id=\"form255\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
      out.write("                                                <div class=\"row\">\r\n");
      out.write("                                                    <div class=\"col-md-12\">\r\n");
      out.write("                                                        <div class=\"errorHandler alert alert-danger no-display\">\r\n");
      out.write("                                                            <i class=\"fa fa-times-sign\"></i> You have some form errors. Please check below.\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                        <div class=\"successHandler alert alert-success no-display\">\r\n");
      out.write("                                                            <i class=\"fa fa-ok\"></i> Your form validation is successful!\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                    </div>\r\n");
      out.write("                                                    <div class=\"col-md-6\">\r\n");
      out.write("                                                    \t<input type=\"hidden\" name=\"id\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                                        <div class=\"form-group\">\r\n");
      out.write("                                                            <input class=\"contributor-id hide\" type=\"text\">\r\n");
      out.write("                                                            <label class=\"control-label\">\r\n");
      out.write("                                                                session title  <span class=\"symbol required\"></span>\r\n");
      out.write("                                                            </label>\r\n");
      out.write("                                                            <input type=\"text\" placeholder=\"Insert a title\" class=\"form-control\" name=\"title\" required=\"true\"  value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.title}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                        <div class=\"form-group\">\r\n");
      out.write("                                                            <input class=\"contributor-id hide\" type=\"date\">\r\n");
      out.write("                                                            <label class=\"control-label\">\r\n");
      out.write("                                                                day  <span class=\"symbol required\"></span>\r\n");
      out.write("                                                            </label>\r\n");
      out.write("                                                            <input type=\"date\" placeholder=\"\" class=\"form-control datepicker\" name=\"date1\" required=\"true\"  value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.startDate}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                        <div class=\"form-group\">\r\n");
      out.write("                                                            <input class=\"contributor-id hide\" type=\"time\">\r\n");
      out.write("                                                            <label class=\"control-label\">\r\n");
      out.write("                                                                from  <span class=\"symbol\"></span>\r\n");
      out.write("                                                            </label>\r\n");
      out.write("                                                            <input type=\"time\" placeholder=\"\" class=\"form-control\" name=\"time1\"  value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.startTime}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                        <div class=\"form-group\">\r\n");
      out.write("                                                            <input class=\"contributor-id hide\" type=\"time\">\r\n");
      out.write("                                                            <label class=\"control-label\">\r\n");
      out.write("                                                                to  <span class=\"symbol\"></span>\r\n");
      out.write("                                                            </label>\r\n");
      out.write("                                                            <input type=\"time\" placeholder=\"\" class=\"form-control\" name=\"time2\"  value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.endTime}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                    </div>\r\n");
      out.write("                                                    <div class=\"col-md-6\">\r\n");
      out.write("                                                        <div>\r\n");
      out.write("                                                            <label for=\"form-field-24\">\r\n");
      out.write("                                                                Description\r\n");
      out.write("                                                            </label>\r\n");
      out.write("                                                            <textarea name=\"description\" class=\"autosize form-control\" id=\"form-field-24\" style=\"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 134px;\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.description}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</textarea>\r\n");
      out.write("                                                        </div>\r\n");
      out.write("\r\n");
      out.write("                                                    </div>\r\n");
      out.write("                                                </div>\r\n");
      out.write("                                                <div class=\"pull-right\">\r\n");
      out.write("                                                    <div class=\"btn-group\">\r\n");
      out.write("                                                        <button class=\"btn btn-info save-contributor\" type=\"submit\">\r\n");
      out.write("                                                            Enregister\r\n");
      out.write("                                                        </button>\r\n");
      out.write("                                                    </div>\r\n");
      out.write("                                                </div>");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</form>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- end: PAGE CONTENT-->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t");
      out.write("<div class=\"subviews\">\r\n");
      out.write("\t\t\t\t\t\t<div class=\"subviews-container\"></div>\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t<!-- end: PAGE -->\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<!-- end: MAIN CONTAINER -->\r\n");
      out.write("\t\t\t<!-- start: FOOTER -->\r\n");
      out.write("\t\t\t<footer class=\"inner\">\r\n");
      out.write("\t\t\t\t<div class=\"footer-inner\">\r\n");
      out.write("\t\t\t\t\t<div class=\"pull-left\">\r\n");
      out.write("\t\t\t\t\t\t2014 &copy; G-Formation.\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t<div class=\"pull-right\">\r\n");
      out.write("\t\t\t\t\t\t<span class=\"go-top\"><i class=\"fa fa-chevron-up\"></i></span>\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t</footer>\r\n");
      out.write("\t\t\t<!-- end: FOOTER -->\r\n");
      out.write("\t\t\t<!-- start: SUBVIEW SAMPLE CONTENTS -->\r\n");
      out.write("\t\t\t<!-- *** NEW NOTE *** -->\r\n");
      out.write("\t\t\t<!-- *** READ NOTE *** -->\r\n");
      out.write("\r\n");
      out.write("\t\t\t<!-- end: SUBVIEW SAMPLE CONTENTS -->\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<!-- start: MAIN JAVASCRIPTS -->\r\n");
      out.write("\t\t<!--[if lt IE 9]>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/respond.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/excanvas.min.js\"></script>\r\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jQuery/jquery-1.11.1.min.js\"></script>\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("\t\t<!--[if gte IE 9]><!-->\r\n");
      out.write("\t\t\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jQuery/jquery-2.1.1.min.js\"></script>\r\n");
      out.write("\t\t<!--<![endif]-->\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap/js/bootstrap.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/blockUI/jquery.blockUI.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/iCheck/jquery.icheck.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/moment/min/moment.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootbox/bootbox.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery.appear/jquery.appear.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-cookie/jquery.cookie.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/velocity/jquery.velocity.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/TouchSwipe/jquery.touchSwipe.min.js\"></script>\r\n");
      out.write("\t\t<!-- end: MAIN JAVASCRIPTS -->\r\n");
      out.write("\t\t<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-mockjax/jquery.mockjax.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/toastr/toastr.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modal.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-select/bootstrap-select.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-validation/dist/jquery.validate.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/js/jquery.dataTables.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/js/DT_bootstrap.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/truncate/jquery.truncate.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/summernote/dist/summernote.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-daterangepicker/daterangepicker.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/subview.js\"></script>\r\n");
      out.write("\t\t<script src0000=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/subview-examples.js\"></script>\r\n");
      out.write("\t\t<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modal.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/confirmDelete/session.js\"></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ckeditor/ckeditor.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ckeditor/adapters/jquery.js\"></script>\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/form-validation.js\"></script>\r\n");
      out.write("\t\t<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->\r\n");
      out.write("\t\t<!-- start: CORE JAVASCRIPTS  -->\r\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/main.js\"></script>\r\n");
      out.write("\t\t<!-- end: CORE JAVASCRIPTS  -->\r\n");
      out.write("\t\t<script>\r\n");
      out.write("\t\t\tjQuery(document).ready(function() {\r\n");
      out.write("\t\t\t\tMain.init();\r\n");
      out.write("\t\t\t\t//SVExamples.init();\r\n");
      out.write("\t\t\t\tFormValidator.init();\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t\t});\r\n");
      out.write("\t\t</script>");
      out.write("\n");
      out.write("\n");
      out.write("\t<script type='text/javascript' src='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/libs/bootstrap-dialog/js/bootstrap-dialog.min.js'></script>\n");
      out.write("\t<script type='text/javascript' src='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/libs/dropzone.js'></script>\n");
      out.write("\t<script type='text/javascript' src='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/js/app.js'></script>\n");
      out.write("\t\n");
      out.write("\n");
      out.write("\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getTrainerInfo.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getUserInfo.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/other/sessionStatus.js\"></script>\n");
      out.write("\t\n");
      out.write("\t\t<script>\n");
      out.write("\t\t\tjQuery(document).ready(function() {\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\tUIModals.init(\"collaborator\");\n");
      out.write("\n");
      out.write("\t\t\t});\n");
      out.write("\t\t</script>\n");
      out.write("\t</body>\n");
      out.write("\t<!-- end: BODY -->\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sw_005fset_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f0.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(11,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(11,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setVar("base_url");
    int _jspx_eval_sw_005fset_005f0 = _jspx_th_sw_005fset_005f0.doStartTag();
    if (_jspx_th_sw_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f1.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(12,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(12,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f1 = _jspx_th_sw_005fset_005f1.doStartTag();
    if (_jspx_th_sw_005fset_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f2.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f2.setParent(null);
    // /WEB-INF/jsp/common/sidebar.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f2.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/sidebar.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f2.setVar("base_url");
    int _jspx_eval_sw_005fset_005f2 = _jspx_th_sw_005fset_005f2.doStartTag();
    if (_jspx_th_sw_005fset_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f2);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f3 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f3.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f3.setParent(null);
    // /WEB-INF/jsp/common/sidebar.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f3.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/sidebar.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f3.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f3 = _jspx_th_sw_005fset_005f3.doStartTag();
    if (_jspx_th_sw_005fset_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f3);
    return false;
  }

  private boolean _jspx_meth_sw_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_sw_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_sw_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fif_005f0.setParent(null);
    // /WEB-INF/jsp/RF/session/show.jsp(119,17) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fif_005f0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.status == 'OnHold'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_sw_005fif_005f0 = _jspx_th_sw_005fif_005f0.doStartTag();
    if (_jspx_eval_sw_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click to open this session)\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
        if (_jspx_meth_sw_005fset_005f4(_jspx_th_sw_005fif_005f0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
        int evalDoAfterBody = _jspx_th_sw_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.reuse(_jspx_th_sw_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.reuse(_jspx_th_sw_005fif_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f4(javax.servlet.jsp.tagext.JspTag _jspx_th_sw_005fif_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f4 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f4.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sw_005fif_005f0);
    // /WEB-INF/jsp/RF/session/show.jsp(121,19) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f4.setVar("btnColor");
    // /WEB-INF/jsp/RF/session/show.jsp(121,19) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f4.setValue(new String("btn-info"));
    int _jspx_eval_sw_005fset_005f4 = _jspx_th_sw_005fset_005f4.doStartTag();
    if (_jspx_th_sw_005fset_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f4);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f4);
    return false;
  }

  private boolean _jspx_meth_sw_005fif_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_sw_005fif_005f1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_sw_005fif_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fif_005f1.setParent(null);
    // /WEB-INF/jsp/RF/session/show.jsp(123,17) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fif_005f1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.status == 'Open'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_sw_005fif_005f1 = _jspx_th_sw_005fif_005f1.doStartTag();
    if (_jspx_eval_sw_005fif_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
        if (_jspx_meth_sw_005fset_005f5(_jspx_th_sw_005fif_005f1, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
        int evalDoAfterBody = _jspx_th_sw_005fif_005f1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fif_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.reuse(_jspx_th_sw_005fif_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fif_0026_005ftest.reuse(_jspx_th_sw_005fif_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f5(javax.servlet.jsp.tagext.JspTag _jspx_th_sw_005fif_005f1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f5 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f5.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sw_005fif_005f1);
    // /WEB-INF/jsp/RF/session/show.jsp(124,19) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f5.setVar("btnColor");
    // /WEB-INF/jsp/RF/session/show.jsp(124,19) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f5.setValue(new String("btn-success"));
    int _jspx_eval_sw_005fset_005f5 = _jspx_th_sw_005fset_005f5.doStartTag();
    if (_jspx_th_sw_005fset_005f5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f5);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f5);
    return false;
  }

  private boolean _jspx_meth_sw_005fchoose_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_sw_005fchoose_005f0 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _005fjspx_005ftagPool_005fsw_005fchoose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_sw_005fchoose_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fchoose_005f0.setParent(null);
    int _jspx_eval_sw_005fchoose_005f0 = _jspx_th_sw_005fchoose_005f0.doStartTag();
    if (_jspx_eval_sw_005fchoose_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t\t    ");
        if (_jspx_meth_sw_005fwhen_005f0(_jspx_th_sw_005fchoose_005f0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("\t\t\t\t");
        int evalDoAfterBody = _jspx_th_sw_005fchoose_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fchoose_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fchoose.reuse(_jspx_th_sw_005fchoose_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fchoose.reuse(_jspx_th_sw_005fchoose_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fwhen_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_sw_005fchoose_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_sw_005fwhen_005f0 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_sw_005fwhen_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fwhen_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sw_005fchoose_005f0);
    // /WEB-INF/jsp/modules/addTrainerToSession.jsp(27,8) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fwhen_005f0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.trainer != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_sw_005fwhen_005f0 = _jspx_th_sw_005fwhen_005f0.doStartTag();
    if (_jspx_eval_sw_005fwhen_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t\t\t\t<tr class=\"element\" element-id=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.trainer.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("\">\r\n");
        out.write("\t\t\t\t\t\t\t<td>");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.trainer.username }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("</td>\r\n");
        out.write("\t\t\t\t\t\t\t<td>");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.trainer.email }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("</td>\r\n");
        out.write("\t\t\t\t\t\t\t<td>");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.trainer.status }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("</td>\r\n");
        out.write("\t\t\t\t\t\t\t<td>\r\n");
        out.write("\t\t\t\t\t\t\t\t<button id=\"buttonRemoveTrainer\">removeTrainer</button>\r\n");
        out.write("\t\t\t\t\t\t\t</td>\r\n");
        out.write("\t\t\t\t\t\t</tr>\r\n");
        out.write("\t\t\t\t    ");
        int evalDoAfterBody = _jspx_th_sw_005fwhen_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fwhen_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.reuse(_jspx_th_sw_005fwhen_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.reuse(_jspx_th_sw_005fwhen_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fforEach_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_sw_005fforEach_005f0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_sw_005fforEach_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fforEach_005f0.setParent(null);
    // /WEB-INF/jsp/modules/addCollaboratorsToSession.jsp(26,5) name = items type = java.lang.Object reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.sessionCollaborators}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/modules/addCollaboratorsToSession.jsp(26,5) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f0.setVar("o");
    int[] _jspx_push_body_count_sw_005fforEach_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_sw_005fforEach_005f0 = _jspx_th_sw_005fforEach_005f0.doStartTag();
      if (_jspx_eval_sw_005fforEach_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("\t\t\t\t\t\t<tr  class=\"element\" element-id=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.collaborator.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\">\r\n");
          out.write("\t\t\t\t\t\t\t\r\n");
          out.write("\t\t\t\t\t\t\t<td class=\"center\"><img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/assets/images/anonymous.jpg\" alt=\"image\" style=\"width:25px; height:25px;\"/></td>\r\n");
          out.write("\t\t\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.collaborator.lastName}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write(' ');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.collaborator.firstName}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\r\n");
          out.write("\t\t\t\t\t\t\t<td class=\"hidden-xs\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.collaborator.username}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\r\n");
          out.write("\t\t\t\t\t\t\t<td class=\"hidden-xs\">\r\n");
          out.write("\t\t\t\t\t\t\t\t<a href=\"#\" rel=\"nofollow\" target=\"_blank\">\r\n");
          out.write("\t\t\t\t\t\t\t\t\t");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.collaborator.email }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\r\n");
          out.write("\t\t\t\t\t\t\t\t</a></td>\r\n");
          out.write("\t\t\t\t\t\t\t<td class=\"hidden-xs\">\r\n");
          out.write("\r\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"isConfirmed\" val=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.confirmed}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\">\r\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
          if (_jspx_meth_sw_005fchoose_005f1(_jspx_th_sw_005fforEach_005f0, _jspx_page_context, _jspx_push_body_count_sw_005fforEach_005f0))
            return true;
          out.write("\r\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n");
          out.write("\r\n");
          out.write("\t\t\t\t\t\t\t</td>\r\n");
          out.write("\t\t\t\t\t\t\t<td class=\"center\">\r\n");
          out.write("\t\t\t\t\t\t\t\t<div class=\"visible-md visible-lg hidden-sm hidden-xs\">\r\n");
          out.write("\t\t\t\t\t\t\t\t\t<button  data-bb=\"confirm\" class=\"btn btn-red tooltips\" data-placement=\"top\" data-original-title=\"Remove\" edit-element-id=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.collaborator.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\" edit-element-idd=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\"><i class=\"fa fa-times fa fa-white\"></i></button>\r\n");
          out.write("\t\t\t\t\t\t\t\t</div>\r\n");
          out.write("\t\t\t\t\t\t\t\t</td>\r\n");
          out.write("\t\t\t\t\t\t</tr>\r\n");
          out.write("\t\t\t\t\t");
          int evalDoAfterBody = _jspx_th_sw_005fforEach_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_sw_005fforEach_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_sw_005fforEach_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_sw_005fforEach_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_sw_005fforEach_005f0.doFinally();
      _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_sw_005fforEach_005f0);
    }
    return false;
  }

  private boolean _jspx_meth_sw_005fchoose_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_sw_005fforEach_005f0, PageContext _jspx_page_context, int[] _jspx_push_body_count_sw_005fforEach_005f0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_sw_005fchoose_005f1 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _005fjspx_005ftagPool_005fsw_005fchoose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_sw_005fchoose_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fchoose_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sw_005fforEach_005f0);
    int _jspx_eval_sw_005fchoose_005f1 = _jspx_th_sw_005fchoose_005f1.doStartTag();
    if (_jspx_eval_sw_005fchoose_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t    ");
        if (_jspx_meth_sw_005fwhen_005f1(_jspx_th_sw_005fchoose_005f1, _jspx_page_context, _jspx_push_body_count_sw_005fforEach_005f0))
          return true;
        out.write("\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t    ");
        if (_jspx_meth_sw_005fotherwise_005f0(_jspx_th_sw_005fchoose_005f1, _jspx_page_context, _jspx_push_body_count_sw_005fforEach_005f0))
          return true;
        out.write("\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
        int evalDoAfterBody = _jspx_th_sw_005fchoose_005f1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fchoose_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fchoose.reuse(_jspx_th_sw_005fchoose_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fchoose.reuse(_jspx_th_sw_005fchoose_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fwhen_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_sw_005fchoose_005f1, PageContext _jspx_page_context, int[] _jspx_push_body_count_sw_005fforEach_005f0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_sw_005fwhen_005f1 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_sw_005fwhen_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fwhen_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sw_005fchoose_005f1);
    // /WEB-INF/jsp/modules/addCollaboratorsToSession.jsp(40,18) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fwhen_005f1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.confirmed == true}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_sw_005fwhen_005f1 = _jspx_th_sw_005fwhen_005f1.doStartTag();
    if (_jspx_eval_sw_005fwhen_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t        confirmed\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t    ");
        int evalDoAfterBody = _jspx_th_sw_005fwhen_005f1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fwhen_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.reuse(_jspx_th_sw_005fwhen_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fwhen_0026_005ftest.reuse(_jspx_th_sw_005fwhen_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fotherwise_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_sw_005fchoose_005f1, PageContext _jspx_page_context, int[] _jspx_push_body_count_sw_005fforEach_005f0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:otherwise
    org.apache.taglibs.standard.tag.common.core.OtherwiseTag _jspx_th_sw_005fotherwise_005f0 = (org.apache.taglibs.standard.tag.common.core.OtherwiseTag) _005fjspx_005ftagPool_005fsw_005fotherwise.get(org.apache.taglibs.standard.tag.common.core.OtherwiseTag.class);
    _jspx_th_sw_005fotherwise_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fotherwise_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sw_005fchoose_005f1);
    int _jspx_eval_sw_005fotherwise_005f0 = _jspx_th_sw_005fotherwise_005f0.doStartTag();
    if (_jspx_eval_sw_005fotherwise_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t        not yet\r\n");
        out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t    ");
        int evalDoAfterBody = _jspx_th_sw_005fotherwise_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sw_005fotherwise_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fotherwise.reuse(_jspx_th_sw_005fotherwise_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fotherwise.reuse(_jspx_th_sw_005fotherwise_005f0);
    return false;
  }
}
