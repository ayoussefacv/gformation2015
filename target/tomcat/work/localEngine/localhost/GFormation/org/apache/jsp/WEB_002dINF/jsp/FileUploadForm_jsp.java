package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class FileUploadForm_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<style type=\"text/css\">\r\n");
      out.write("body {\r\n");
      out.write("    background-image:\r\n");
      out.write("        url('http://cdn3.crunchify.com/wp-content/uploads/2013/03/Crunchify.bg_.300.png');\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("    <br>\r\n");
      out.write("    <br>\r\n");
      out.write("    <div align=\"center\">\r\n");
      out.write(" \r\n");
      out.write("        <h1>Crunchify - Spring MVC Upload Multiple Files Example</h1>\r\n");
      out.write("        <p>Awesome.. Following files are uploaded successfully.</p>\r\n");
      out.write("        <ol>\r\n");
      out.write("            <c:forEach items=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${files}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\" var=\"file\">\r\n");
      out.write("           - ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${file}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" <br>\r\n");
      out.write("            </c:forEach>\r\n");
      out.write("        </ol>\r\n");
      out.write("        <a href=\"http://localhost:8080/CrunchifySpringMVC3.2.1/upload.html\"><input\r\n");
      out.write("            type=\"button\" value=\"Go Back\" /></a> <br />\r\n");
      out.write("        <br />\r\n");
      out.write("        <br />\r\n");
      out.write("        <div\r\n");
      out.write("            style=\"font-family: verdana; line-height: 25px; padding: 5px 10px; border-radius: 10px; border: 1px dotted #A4A4A4; width: 50%; font-size: 12px;\">\r\n");
      out.write(" \r\n");
      out.write("            Spring MVC Upload Multiple Files Example by <a\r\n");
      out.write("                href='http://crunchify.com'>Crunchify</a>. Click <a\r\n");
      out.write("                href='http://crunchify.com/category/java-web-development-tutorial/'>here</a>\r\n");
      out.write("            for all Java, Spring MVC, Web Development examples.<br>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
