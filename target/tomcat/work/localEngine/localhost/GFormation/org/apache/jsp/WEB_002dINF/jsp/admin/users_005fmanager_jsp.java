package org.apache.jsp.WEB_002dINF.jsp.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class users_005fmanager_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(3);
    _jspx_dependants.add("/WEB-INF/jsp/common/header.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/common/sidebar.jsp");
    _jspx_dependants.add("/WEB-INF/jsp/common/footer.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write(" \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 if (request.getAttribute("error") != null) { 
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t     \t\t\t ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${error}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write('\n');
 } 
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->\n");
      out.write("<!--[if IE 8]><html class=\"ie8\" lang=\"en\"><![endif]-->\n");
      out.write("<!--[if IE 9]><html class=\"ie9\" lang=\"en\"><![endif]-->\n");
      out.write("<!--[if !IE]><!-->\n");
      out.write("<html lang=\"en\">\n");
      out.write("\t<!--<![endif]-->\n");
      out.write("\t<!-- start: HEAD -->\n");
      out.write("\t<head>\n");
      out.write("\t\t<title>G-Formation</title>\n");
      out.write("\t\t<!-- start: META -->\n");
      out.write("\t\t<meta charset=\"utf-8\" />\n");
      out.write("\t\t<!--[if IE]><meta http-equiv='X-UA-Compatible' content=\"IE=edge,IE=9,IE=8,chrome=1\" /><![endif]-->\n");
      out.write("\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\">\n");
      out.write("\t\t<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n");
      out.write("\t\t<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n");
      out.write("\t\t<meta content=\"\" name=\"description\" />\n");
      out.write("\t\t<meta content=\"\" name=\"author\" />\n");
      out.write("\t\t<!-- end: META -->\n");
      out.write("\t\t<!-- start: MAIN CSS -->\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap/css/bootstrap.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/font-awesome/css/font-awesome.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/iCheck/skins/all.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/animate.css/animate.min.css\">\n");
      out.write("\t\t<!-- end: MAIN CSS -->\n");
      out.write("\t\t<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.theme.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.transitions.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/summernote/dist/summernote.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/toastr/toastr.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-select/bootstrap-select.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/css/DT_bootstrap.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css\">\n");
      out.write("\t\t<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->\n");
      out.write("\t\t<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- start: CORE CSS -->\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/styles.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/styles-responsive.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/plugins.css\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/themes/theme-default.css\" type=\"text/css\" id=\"skin_color\">\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/css/print.css\" type=\"text/css\" media=\"print\"/>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/autocomplete/getUserInfo.css\">\n");
      out.write("\t\t<!-- end: CORE CSS -->\n");
      out.write("\t\t<link rel=\"shortcut icon\" href=\"favicon.ico\" />\n");
      out.write("\t\t<!-- gformation  -->\n");
      out.write("\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\t\t var base_url = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";\t\n");
      out.write("\t\t\t\t var session = new Object();\n");
      out.write("\t\t \t\t\tsession.id = \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${session.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\";\n");
      out.write("</script>\n");
      out.write("\t</head>\n");
      out.write("\t<!-- end: HEAD -->\n");
      out.write("\t<!-- start: BODY -->\n");
      out.write("\t<body>\n");
      out.write("\t\t<!-- start: SLIDING BAR (SB) -->\n");
      out.write("\t\t<!-- end: SLIDING BAR -->\n");
      out.write("\t\t<div class=\"main-wrapper\">\n");
      out.write("\t\t\t<!-- start: TOPBAR -->\n");
      out.write("\t\t\t<header class=\"topbar navbar navbar-inverse navbar-fixed-top inner\">\n");
      out.write("\t\t\t\t<!-- start: TOPBAR CONTAINER -->\n");
      out.write("\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t<div class=\"navbar-header\">\n");
      out.write("\t\t\t\t\t\t<a class=\"sb-toggle-left hidden-md hidden-lg\" href=\"#main-navbar\">\n");
      out.write("\t\t\t\t\t\t\t<i class=\"fa fa-bars\"></i>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t<!-- start: LOGO -->\n");
      out.write("\t\t\t\t\t\t<a class=\"navbar-brand\" href=\"index.html\">\n");
      out.write("\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/logo.png\" alt=\"Rapido\"/>\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t<!-- end: LOGO -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"topbar-tools\">\n");
      out.write("\t\t\t\t\t\t<!-- start: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"nav navbar-right\">\n");
      out.write("\t\t\t\t\t\t\t<!-- start: USER DROPDOWN -->\n");
      out.write("\t\t\t\t\t\t\t<li class=\"dropdown current-user\">\n");
      out.write("\t\t\t\t\t\t\t\t<a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" data-close-others=\"true\" href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t\t<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/anonymous.jpg\" width=\"25\" class=\"img-circle\" alt=\"\"> <span class=\"username hidden-xs\">Mr. Admin</span> \n");
      out.write("\t\t\t\t\t\t\t\t\t <i class=\"fa fa-caret-down \"></i>\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-dark\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/login?logout\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tLog Out\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t<!-- end: USER DROPDOWN -->\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t<!-- end: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end: TOPBAR CONTAINER -->\n");
      out.write("\t\t\t</header>");
      out.write('\n');
      out.write("\n");
      out.write(" \n");
      out.write("  \n");
      if (_jspx_meth_sw_005fset_005f2(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_sw_005fset_005f3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t<!-- start: PAGESLIDE LEFT -->\n");
      out.write("\t\t\t<a class=\"closedbar inner hidden-sm hidden-xs\" href=\"#\">\n");
      out.write("\t\t\t</a>\n");
      out.write("\t\t\t<nav id=\"pageslide-left\" class=\"pageslide inner\">\n");
      out.write("\t\t\t\t<div class=\"navbar-content\">\n");
      out.write("\t\t\t\t\t<!-- start: SIDEBAR -->\n");
      out.write("\t\t\t\t\t<div class=\"main-navigation left-wrapper transition-left\">\n");
      out.write("\t\t\t\t\t\t<div class=\"navigation-toggler hidden-sm hidden-xs\">\n");
      out.write("\t\t\t\t\t\t\t<a href=\"#main-navbar\" class=\"sb-toggle-left\">\n");
      out.write("\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"user-profile border-top padding-horizontal-10 block\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"inline-block\">\n");
      out.write("\t\t\t\t\t\t\t\t<img width=\"45px\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/images/anonymous.jpg\" alt=\"\">\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"inline-block\">\n");
      out.write("\t\t\t\t\t\t\t\t<h5 class=\"no-margin\"> Bienvenu </h5>\n");
      out.write("\t\t\t\t\t\t\t\t<h4 class=\"no-margin\"> Mr. Admin </h4>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- start: MAIN NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"main-navigation-menu\">\n");
      out.write("\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"index.html\"><i class=\"fa fa-home\"></i> <span class=\"title\"> Acceuil </span><span class=\"label label-default pull-right \">Home</span> </a>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-desktop\"></i> <span class=\"title\"> Administration </span><i class=\"icon-arrow\"></i> </a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tUtilisateurs <i class=\"icon-arrow\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_menu.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tAjouter\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tGroups <i class=\"icon-arrow\"></i>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts_horizontal_menu.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tAjouter\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t<li class=\"active open\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-code\"></i> <span class=\"title\">Formations</span><i class=\"icon-arrow\"></i> </a>\n");
      out.write("\t\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"pages_user_profile.html\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Liste Formations</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t<!-- end: MAIN NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- end: SIDEBAR -->\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"slide-tools\">\n");
      out.write("\t\t\t\t\t<div class=\"col-xs-6 text-left no-padding\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"col-xs-6 text-right no-padding\">\n");
      out.write("\t\t\t\t\t\t<a class=\"btn btn-sm log-out text-right\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/login?logout\">\n");
      out.write("\t\t\t\t\t\t\t<i class=\"fa fa-power-off\"></i> Log Out\n");
      out.write("\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</nav>\n");
      out.write("\t\t\t<!-- end: PAGESLIDE LEFT -->");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t<!-- start: MAIN CONTAINER -->\n");
      out.write("\t\t\t<div class=\"main-container inner\">\n");
      out.write("\t\t\t\t<!-- start: PAGE -->\n");
      out.write("\t\t\t\t<div class=\"main-content\">\n");
      out.write("\t\t\t\t\t<!-- start: PANEL CONFIGURATION MODAL FORM -->\n");
      out.write("\t\t\t\t\t<div class=\"modal fade\" id=\"panel-config\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n");
      out.write("\t\t\t\t\t\t<div class=\"modal-dialog\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"modal-content\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t&times;\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t<h4 class=\"modal-title\">Panel Configuration</h4>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\tHere will be a configuration form\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"modal-footer\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tClose\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tSave changes\n");
      out.write("\t\t\t\t\t\t\t\t\t</button>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<!-- /.modal-content -->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- /.modal-dialog -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!-- /.modal -->\n");
      out.write("\t\t\t\t\t<!-- end: SPANEL CONFIGURATION MODAL FORM -->\n");
      out.write("\t\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t\t<!-- start: PAGE HEADER -->\n");
      out.write("\t\t\t\t\t\t<!-- start: TOOLBAR -->\n");
      out.write("\t\t\t\t\t\t<div class=\"toolbar row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-sm-6 hidden-xs\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"page-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<h1>Blank Page <small>subtitle here</small></h1>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"back-subviews\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"fa fa-chevron-left\"></i> BACK\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"close-subviews\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i> CLOSE\n");
      out.write("\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"toolbar-tools pull-right\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<!-- start: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t\t\t\t<ul class=\"nav navbar-right\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<!-- start: TO-DO DROPDOWN -->\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" data-close-others=\"true\" href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-plus\"></i> SUBVIEW\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tooltip-notification hide\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tooltip-notification-arrow\"></div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-light dropdown-subview\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tNotes\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#newNote\" class=\"new-note\"><span class=\"fa-stack\"> <i class=\"fa fa-file-text-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-plus fa-stack-1x stack-right-bottom text-danger\"></i> </span> Add new note</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#readNote\" class=\"read-all-notes\"><span class=\"fa-stack\"> <i class=\"fa fa-file-text-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-share fa-stack-1x stack-right-bottom text-danger\"></i> </span> Read all notes</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tCalendar\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#newEvent\" class=\"new-event\"><span class=\"fa-stack\"> <i class=\"fa fa-calendar-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-plus fa-stack-1x stack-right-bottom text-danger\"></i> </span> Add new event</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#showCalendar\" class=\"show-calendar\"><span class=\"fa-stack\"> <i class=\"fa fa-calendar-o fa-stack-1x fa-lg\"></i> <i class=\"fa fa-share fa-stack-1x stack-right-bottom text-danger\"></i> </span> Show calendar</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"dropdown-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\tContributors\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#newContributor\" class=\"new-contributor\"><span class=\"fa-stack\"> <i class=\"fa fa-user fa-stack-1x fa-lg\"></i> <i class=\"fa fa-plus fa-stack-1x stack-right-bottom text-danger\"></i> </span> Add new contributor</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#showContributors\" class=\"show-contributors\"><span class=\"fa-stack\"> <i class=\"fa fa-user fa-stack-1x fa-lg\"></i> <i class=\"fa fa-share fa-stack-1x stack-right-bottom text-danger\"></i> </span> Show all contributor</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t<!-- end: TOP NAVIGATION MENU -->\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- end: TOOLBAR -->\n");
      out.write("\t\t\t\t\t\t<!-- end: PAGE HEADER -->\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("<!-- start: BREADCRUMB -->\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<ol class=\"breadcrumb\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"#\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\tDashboard\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t<li class=\"active\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tBlank Page\n");
      out.write("\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t</ol>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!-- end: BREADCRUMB -->\n");
      out.write("\t\t\t\t\t\t<!-- start: PAGE CONTENT -->\n");
      out.write("\t\t\t\t\t<div class=\"row\" id=\"collaboratorsListContainer\">\n");
      out.write("\n");
      out.write("\t\t<div class=\"col-md-12\">\n");
      out.write("\t\t\t\t\t\t\t\t<!-- start: BASIC TABLE PANEL -->\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"panel panel-white\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">Utilisateur <span class=\"text-bold\"> table</span></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"pull-right\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"btn btn-info save-contributor\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${base_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/admin/users_manager/add_user\" >\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAjouter un Utilisateur\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<hr>\n");
      out.write("\t\t\t<table class=\"table table-striped table-bordered table-hover\" id=\"listSesssionUsers\">\n");
      out.write("\t\t\t\t<thead>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<th class=\"center\"></th>\n");
      out.write("\t\t\t\t\t<th>Full Name</th>\n");
      out.write("\t\t\t\t\t<th class=\"hidden-xs\">Username</th>\n");
      out.write("\t\t\t\t\t<th class=\"hidden-xs\">Email</th>\n");
      out.write("\t\t\t\t\t<th></th>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t</thead>\n");
      out.write("\t\t\t\t<tbody>\n");
      out.write("\t\t\t\t\t");
      if (_jspx_meth_sw_005fforEach_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t\t\t</tbody>\n");
      out.write("\t\t\t</table>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<!-- ====================================================================================================== -->\n");
      out.write("\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t<!-- end: PAGE CONTENT-->\n");
      out.write("\n");
      out.write("<div class=\"subviews\">\n");
      out.write("\t\t\t\t\t\t<div class=\"subviews-container\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end: PAGE -->\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- end: MAIN CONTAINER -->\n");
      out.write("\t\t\t<!-- start: FOOTER -->\n");
      out.write("\t\t\t<footer class=\"inner\">\n");
      out.write("\t\t\t\t<div class=\"footer-inner\">\n");
      out.write("\t\t\t\t\t<div class=\"pull-left\">\n");
      out.write("\t\t\t\t\t\t2014 &copy; G-Formation.\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"pull-right\">\n");
      out.write("\t\t\t\t\t\t<span class=\"go-top\"><i class=\"fa fa-chevron-up\"></i></span>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</footer>\n");
      out.write("\t\t\t<!-- end: FOOTER -->\n");
      out.write("\t\t\t<!-- start: SUBVIEW SAMPLE CONTENTS -->\n");
      out.write("\t\t\t<!-- *** NEW NOTE *** -->\n");
      out.write("\t\t\t<!-- *** READ NOTE *** -->\n");
      out.write("\n");
      out.write("\t\t\t<!-- end: SUBVIEW SAMPLE CONTENTS -->\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<!-- start: MAIN JAVASCRIPTS -->\n");
      out.write("\t\t<!--[if lt IE 9]>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/respond.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/excanvas.min.js\"></script>\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jQuery/jquery-1.11.1.min.js\"></script>\n");
      out.write("\t\t<![endif]-->\n");
      out.write("\t\t<!--[if gte IE 9]><!-->\n");
      out.write("\t\t\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jQuery/jquery-2.1.1.min.js\"></script>\n");
      out.write("\t\t<!--<![endif]-->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap/js/bootstrap.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/blockUI/jquery.blockUI.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/iCheck/jquery.icheck.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/moment/min/moment.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootbox/bootbox.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery.appear/jquery.appear.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-cookie/jquery.cookie.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/velocity/jquery.velocity.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/TouchSwipe/jquery.touchSwipe.min.js\"></script>\n");
      out.write("\t\t<!-- end: MAIN JAVASCRIPTS -->\n");
      out.write("\t\t<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-mockjax/jquery.mockjax.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/toastr/toastr.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modal.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-select/bootstrap-select.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/jquery-validation/dist/jquery.validate.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/js/jquery.dataTables.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/DataTables/media/js/DT_bootstrap.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/truncate/jquery.truncate.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/summernote/dist/summernote.min.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-daterangepicker/daterangepicker.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/subview.js\"></script>\n");
      out.write("\t\t<script src0000=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/subview-examples.js\"></script>\n");
      out.write("\t\t<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modal.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/special/confirmDelete/session.js\"></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\n");
      out.write("\t\t<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ckeditor/ckeditor.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/plugins/ckeditor/adapters/jquery.js\"></script>\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/form-validation.js\"></script>\n");
      out.write("\t\t<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->\n");
      out.write("\t\t<!-- start: CORE JAVASCRIPTS  -->\n");
      out.write("\t\t<script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("/assets/js/main.js\"></script>\n");
      out.write("\t\t<!-- end: CORE JAVASCRIPTS  -->\n");
      out.write("\t\t<script>\n");
      out.write("\t\t\tjQuery(document).ready(function() {\n");
      out.write("\t\t\t\tMain.init();\n");
      out.write("\t\t\t\t//SVExamples.init();\n");
      out.write("\t\t\t\tFormValidator.init();\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t});\n");
      out.write("\t\t</script>");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sw_005fset_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f0.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(11,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(11,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f0.setVar("base_url");
    int _jspx_eval_sw_005fset_005f0 = _jspx_th_sw_005fset_005f0.doStartTag();
    if (_jspx_th_sw_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f0);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f1.setParent(null);
    // /WEB-INF/jsp/common/header.jsp(12,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/header.jsp(12,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f1.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f1 = _jspx_th_sw_005fset_005f1.doStartTag();
    if (_jspx_th_sw_005fset_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f1);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f2.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f2.setParent(null);
    // /WEB-INF/jsp/common/sidebar.jsp(5,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f2.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/sidebar.jsp(5,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f2.setVar("base_url");
    int _jspx_eval_sw_005fset_005f2 = _jspx_th_sw_005fset_005f2.doStartTag();
    if (_jspx_th_sw_005fset_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f2);
    return false;
  }

  private boolean _jspx_meth_sw_005fset_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_sw_005fset_005f3 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_sw_005fset_005f3.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fset_005f3.setParent(null);
    // /WEB-INF/jsp/common/sidebar.jsp(6,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f3.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}/resources", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/common/sidebar.jsp(6,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fset_005f3.setVar("resources_url");
    int _jspx_eval_sw_005fset_005f3 = _jspx_th_sw_005fset_005f3.doStartTag();
    if (_jspx_th_sw_005fset_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fsw_005fset_0026_005fvar_005fvalue_005fnobody.reuse(_jspx_th_sw_005fset_005f3);
    return false;
  }

  private boolean _jspx_meth_sw_005fforEach_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sw:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_sw_005fforEach_005f0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_sw_005fforEach_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sw_005fforEach_005f0.setParent(null);
    // /WEB-INF/jsp/admin/users_manager.jsp(150,5) name = items type = java.lang.Object reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${users}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/jsp/admin/users_manager.jsp(150,5) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sw_005fforEach_005f0.setVar("o");
    int[] _jspx_push_body_count_sw_005fforEach_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_sw_005fforEach_005f0 = _jspx_th_sw_005fforEach_005f0.doStartTag();
      if (_jspx_eval_sw_005fforEach_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t\t\t\t\t<tr  class=\"element\" element-id=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\">\n");
          out.write("\t\t\t\t\t\t\t\n");
          out.write("\t\t\t\t\t\t\t<td class=\"center\"><img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resources_url}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("/assets/images/anonymous.jpg\" alt=\"image\" style=\"width:25px; height:25px;\"/></td>\n");
          out.write("\t\t\t\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.lastName}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write(' ');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.firstName}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t\t\t<td class=\"hidden-xs\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.username}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("</td>\n");
          out.write("\t\t\t\t\t\t\t<td class=\"hidden-xs\">\n");
          out.write("\t\t\t\t\t\t\t\t<a href=\"#\" rel=\"nofollow\" target=\"_blank\">\n");
          out.write("\t\t\t\t\t\t\t\t\t");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.email }", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\n");
          out.write("\t\t\t\t\t\t\t\t</a></td>\n");
          out.write("\t\t\t\t\t\t\t\n");
          out.write("\t\t\t\t\t\t\t<td class=\"center\">\n");
          out.write("\t\t\t\t\t\t\t\t<div class=\"visible-md visible-lg hidden-sm hidden-xs\">\n");
          out.write("\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"btn btn-xs btn-red tooltips\" data-placement=\"top\" data-original-title=\"Remove\"><i class=\"fa fa-times fa fa-white\"></i></a>\n");
          out.write("\t\t\t\t\t\t\t\t</div>\n");
          out.write("\t\t\t\t\t\t\t\t<div class=\"visible-xs visible-sm hidden-md hidden-lg\">\n");
          out.write("\t\t\t\t\t\t\t\t\t<div class=\"btn-group\">\n");
          out.write("\t\t\t\t\t\t\t\t\t\t<a class=\"btn btn-green dropdown-toggle btn-sm\" data-toggle=\"dropdown\" href=\"#\">\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-cog\"></i> <span class=\"caret\"></span>\n");
          out.write("\t\t\t\t\t\t\t\t\t\t</a>\n");
          out.write("\t\t\t\t\t\t\t\t\t\t<ul role=\"menu\" class=\"dropdown-menu pull-right dropdown-dark\">\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"\">\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i> Remove\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t\t</a>\n");
          out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
          out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
          out.write("\t\t\t\t\t\t\t\t\t</div>\n");
          out.write("\t\t\t\t\t\t\t\t</div></td>\n");
          out.write("\t\t\t\t\t\t</tr>\n");
          out.write("\t\t\t\t\t");
          int evalDoAfterBody = _jspx_th_sw_005fforEach_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_sw_005fforEach_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_sw_005fforEach_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_sw_005fforEach_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_sw_005fforEach_005f0.doFinally();
      _005fjspx_005ftagPool_005fsw_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_sw_005fforEach_005f0);
    }
    return false;
  }
}
