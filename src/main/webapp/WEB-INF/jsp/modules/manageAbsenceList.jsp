	<div id="collaboratorsListContainer">
		<table class="table table-striped table-bordered table-hover" id="listSesssionUsers">
			<thead>
			<tr>
				<th style="width:35px"></th>
				<th>Full Name</th>
				<th class="hidden-xs">Username</th>
				<th class="hidden-xs">Email</th>
				<th class="hidden-xs">absent ?</th>
			</tr>
			</thead>
			<tbody>
				<sw:forEach items="${session.sessionCollaborators}" var="o" >
					<tr  class="element" element-id="${o.collaborator.id}">

						<td ><img  src="${resources_url}/assets/images/anonymous.jpg" alt="image" style="width:25px"/></td>

						<td>${o.collaborator.lastName} ${o.collaborator.firstName}</td>
						<td class="hidden-xs">${o.collaborator.username}</td>
						<td class="hidden-xs">
							<a href="#" rel="nofollow" target="_blank">
								${o.collaborator.email }
							</a></td>
						<td class="hidden-xs">

											<button class="isAbsent" val="${o.absent}">
													<sw:choose>
													    <sw:when test="${o.absent == true}">
													        absent
													    </sw:when>
													    <sw:otherwise>
													        .
													    </sw:otherwise>
													</sw:choose>
											</button>

						</td>
					</tr>
				</sw:forEach>
			</tbody>
		</table>
		<!-- ====================================================================================================== -->

	</div>


