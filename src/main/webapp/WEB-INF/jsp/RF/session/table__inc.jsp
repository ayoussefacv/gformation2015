	<div id="collaboratorsListContainer">
		<div class="contentArea">
			<input type="text" class="search" id="inputSearchUser" /> put username<br /> 
			<div id="divResultSearchUser">
			</div>
		</div>
		<table class="table table-striped table-bordered table-hover" id="listSesssionUsers">
												<thead>
													<tr>
														<th class="center">
														<div class="checkbox-table">
															<label>
																<input type="checkbox" class="flat-grey selectall">
															</label>
														</div></th>
														<th>Project Name</th>
														<th class="hidden-xs">Client</th>
														<th>Proj Comp</th>
														<th class="hidden-xs">%Comp</th>
														<th class="hidden-xs center">Priority</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="center">
														<div class="checkbox-table">
															<label>
																<input type="checkbox" class="flat-grey foocheck">
															</label>
														</div></td>
														<td>IT Help Desk</td>
														<td class="hidden-xs">Master Company</td>
														<td>11 november 2014</td>
														<td class="hidden-xs">
														<div class="progress active progress-xs">
															<div style="width: 70%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" role="progressbar" class="progress-bar progress-bar-warning">
																<span class="sr-only"> 70% Complete (danger)</span>
															</div>
														</div></td>
														<td class="center hidden-xs"><span class="label label-danger">Critical</span></td>
														<td class="center">
														<div class="visible-md visible-lg hidden-sm hidden-xs">
															<a href="#" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
															<a href="#" class="btn btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
															<a href="#" class="btn btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
														</div>
														<div class="visible-xs visible-sm hidden-md hidden-lg">
															<div class="btn-group">
																<a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
																	<i class="fa fa-cog"></i> <span class="caret"></span>
																</a>
																<ul role="menu" class="dropdown-menu dropdown-dark pull-right">
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="#">
																			<i class="fa fa-edit"></i> Edit
																		</a>
																	</li>
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="#">
																			<i class="fa fa-share"></i> Share
																		</a>
																	</li>
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="#">
																			<i class="fa fa-times"></i> Remove
																		</a>
																	</li>
																</ul>
															</div>
														</div></td>
													</tr>
								
												</tbody>
											</table>