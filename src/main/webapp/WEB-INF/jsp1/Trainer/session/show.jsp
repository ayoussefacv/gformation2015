<%@ include file="/WEB-INF/jsp/common/header.jsp" %>

	<h1>Training ${session.training.id }</h1>
	<ul>
		<li>session_id : ${session.id}</li>
		<li>title : ${session.title}</li>
		<li>description : ${session.description}</li>
		<li>startDate : ${session.startDate}</li>
		<li>endDate${session.endDate}</li>
	</ul>
	<h2>List of sessions</h2>
	<hr />
	<a href="${base_url}/RF/sessions/${session.id}/update">update session</a>
	<hr />
	Session collaborators:
	<table border="1" width="50%"  id="listSesssionUsers">
		<tr>
			<th>username</th>
			<th>email</th>
			<th>status</th>
			<th>absent ?</th>
		</tr>

			<sw:forEach items="${session.sessionCollaborators}" var="o">
				<tr class="element" element-id="${o.collaborator.id}">
					<td>${o.collaborator.username }</td>
					<td>${o.collaborator.email }</td>
					<td>${o.collaborator.status }</td>
					<td><button class="isAbsent" val="${o.absent}">
							<sw:choose>
							    <sw:when test="${o.absent == true}">
							        yes
							    </sw:when>
							    <sw:otherwise>
							        .
							    </sw:otherwise>
							</sw:choose>
					</button></td>
				</tr>
			</sw:forEach>
	</table>
	<script type="text/javascript">
		var collaborator = new Object();
		$(".isAbsent").click(function(){
			var button=$(this);
			collaborator.id = $(this).parent().parent().attr('element-id');
			var absenceStatus = button.attr("val");
			console.log("absenceStatus="+absenceStatus);
			var newAbsenceStatus ;
			if(absenceStatus == "true")
			{
				newAbsenceStatus = "false";
				absenceMessage = ".";
			}
			else
			{
				newAbsenceStatus = "true";
				absenceMessage = "yes";
			}
			// add user to list :
			var dataString = 'collaboratorId='+ collaborator.id;
			if(collaborator.id!='')
			{
				$.ajax({
				type: "POST",
				url: base_url+"/Trainer/sessions/"+session.id +"/setAbscenceList/"+ collaborator.id+'/'+newAbsenceStatus,
				data: dataString,
				cache: false,
				success: function(html)
				{
					button.attr("val",newAbsenceStatus);
					button.html(absenceMessage);
					console.log(html);
				}
				});
			}
		});

	</script>
	<br/>
	<table border="1" width="50%"  id="listSesssionDocuents">
		<tr>
			<th>name</th>
			<th>options</th>
		</tr>
		
		<div >
			<sw:forEach items="${session.documents}" var="o" >
				<tr class="element" element-id="${o.id}">
					<td>${o.name }</td>
					<td>
						<a href="${base_url}/get/${o.id}">download</a>
						<a href="${base_url}/documents/${o.id}/delete">delete</a>
					</td>
				</tr>
			</sw:forEach>
		</div>
	</table>

	<div class="fileUploder">
		<%@ include file="/WEB-INF/jsp/modules/fileUploader.jsp" %>
	</div>

	<script type="text/javascript">
			var session=new Object();
				session.id=${session.id};

	</script>
</body>
</html>