package com.GFormation.core.dao;

import com.GFormation.core.model.Group;

public interface GroupDao  extends DAO<Group>{
	Group getByTitle(String string);
}
