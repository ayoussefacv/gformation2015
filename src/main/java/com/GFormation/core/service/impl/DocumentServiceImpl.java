package com.GFormation.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.GFormation.core.dao.DocumentDao;
import com.GFormation.core.model.Document;
import com.GFormation.core.model.Session;
import com.GFormation.core.service.DocumentService;

public class DocumentServiceImpl implements DocumentService {

  @Autowired
  private DocumentDao dao;

  @Override
  @Transactional(readOnly = true)
  public List<Document> listFiles() {
    return dao.listFiles();
  }


  @Override
  @Transactional(readOnly = true)
  public Document getFile(Long id) {
    return dao.getFile(id);
  }

  @Override
  @Transactional
  public Document saveFile(Document uploadedFile) {
    return dao.saveFile(uploadedFile);

  }


	@Override
	@Transactional
	public boolean isExist(Document document, Session session) {

	    return dao.isExist(document , session);
	}

}
