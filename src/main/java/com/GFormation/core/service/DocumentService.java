package com.GFormation.core.service;

import java.util.List;

import com.GFormation.core.model.Document;
import com.GFormation.core.model.Session;

public interface DocumentService{

	  List<Document> listFiles();

	  Document getFile(Long id);

	  Document saveFile(Document uploadedFile);
	  
	  boolean isExist(Document document, Session session);

}
