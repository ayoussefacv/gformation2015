package com.GFormation.core.service;

import java.util.List;
import java.util.Set;

import com.GFormation.core.model.Session;

public interface SessionService{

	boolean addCollaboratorToSession(long collaboratorId, long sessionId);

	boolean removeCollaborator(int sessionId, int collaboratorId);

	boolean addTrainerToSession(long trainerId, long sessionId);

	boolean removeTrainer(int sessionId);

	List<Session> getTrainerSessions(long trainerId);

	List<Session> getCollaboratorSessions(long collaboratorId);

}
