package com.GFormation.core.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Table;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="Document")
public class Document  implements Serializable{

  private Long id;
  private String name;
  private String nameInDisk;
  private String location;
  private Long size;
  private String type;  
  private Session session;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }
  
  @ManyToOne(cascade=CascadeType.ALL , fetch=FetchType.LAZY)
  @JoinColumn(name="session_id")
  @JsonManagedReference
  @NaturalId
  public Session getSession() {
 	return session;
  }
	
  public void setSession(Session session) {
	this.session = session;
  }

  @NaturalId
  @Column(nullable = false)
  public String getName() {
    return name;
  }
  
  @Column(nullable = false)
  public String getLocation() {
    return location;
  }

  @Column()
  public Long getSize() {
    return size;
  }

  @Column(nullable = false)
  public String getType() {
    return type;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Column(nullable = false)
  public String getNameInDisk() {
	return nameInDisk;
  }
	
   public void setNameInDisk(String nameInDisk) {
		this.nameInDisk = nameInDisk;
  }

}
