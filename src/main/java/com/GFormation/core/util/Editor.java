package com.GFormation.core.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.config.SqlTimestampPropertyEditor;
import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.model.Training;
import com.GFormation.core.model.User;


@Controller
@RequestMapping(value="RF/trainings")
public final class Editor {

	public static Date stringToDate(String dateString)
	{
		Timestamp ts = Timestamp.valueOf(dateString+" 00:00:00");
	    Date date = new Date(ts.getTime());  // wherever you get this
	    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	    String text = df.format(date);
	    //System.out.println(text);
	    return date;
	}
	public static Date stringToTime(String dateString)
	{
		System.out.println("danger="+dateString);
		Timestamp ts ;
		if(dateString.length()<=5)
			 ts = Timestamp.valueOf("1970-01-01 "+dateString+":00");
		else
			 ts = Timestamp.valueOf("1970-01-01 "+dateString+"");
	    Date date = new Date(ts.getTime());  // wherever you get this
	    DateFormat df = new SimpleDateFormat("hh:mm");
	    String text = df.format(date);
	    //System.out.println(text);
	    return date;
	}

	public static String generateEmailBody(SessionCollaborator sessionCollaborator , String base_url)
	{
		
		String confirmationCode=sessionCollaborator.getConfirmationCode();
		User collaborator = sessionCollaborator.getCollaborator();
		Session session = sessionCollaborator.getSession();
		
		String body = "Hello "+collaborator.getLastName()+" "+collaborator.getFirstName()+"\n";
		body+=" a new session is going to start the "+session.getStartDate()+" from "+session.getStartTime()+" to "+session.getEndTime()+"\n";
		body+= "under the title :"+session.getTitle()+""+"\n";
		body+= "to confirm your inscreption click here : "+base_url+"/confirm/sessionId/"+session.getId()+"/collaboratorId/"+collaborator.getId()+"/code/"+confirmationCode+" "+"\n";
		body+= "to see more details click this link :"+base_url+"/collaborator/sessions/"+session.getId()+""+"\n";

		return body;
	}
}
