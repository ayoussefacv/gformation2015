package com.GFormation.core.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.User;


@Controller
@RequestMapping(value="/admin")
public class AdminController {
	private static final Logger logger =LoggerFactory.getLogger(AdminController.class);

	@Autowired
	UserDao userDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;


	
	@RequestMapping({"/admin"})
	public ModelAndView homeIndexPage(){
		
		logger.info("entering  /admin/DashBoard");
		return new ModelAndView("/admin/DashBoard");
	}
	
	@RequestMapping(value="/users_manager", method=RequestMethod.GET)
	public ModelAndView users_managerPage() {
		
		logger.info("users_managerPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("admin/users_manager");
		
		List<User> users = userDAO.getAll();
		modelAndView.addObject("users", users);
		return modelAndView;
	}
	
	@RequestMapping(value="/users_manager", method=RequestMethod.POST)
	public ModelAndView users_manager_add_userPage(@ModelAttribute("User")User user, @RequestParam long group_id,
			   ModelMap model){
		
		
		ModelAndView modelAndView = new ModelAndView("admin/users_manager");
		PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
		String encodedPassword = passwordEncoder.encodePassword(user.getPassword(),null);
		
		Group group2 = groupDAO.getById(group_id);
		User newuser = new User();
		newuser.setFirstName(user.getFirstName());
		newuser.setLastName(user.getLastName());
		newuser.setEmail(user.getEmail());
		newuser.setUsername(user.getUsername());
		newuser.setPassword(encodedPassword);
		newuser.setGroup(group2);
		
		userDAO.add(newuser);
		

		List<User> users = userDAO.getAll();
		modelAndView.addObject("users", users);
		return modelAndView;
	}
	
	@RequestMapping(value="/users_manager/add_user", method=RequestMethod.GET)
	public String add_userPage(ModelMap map) {
		
		logger.info("groups_managerPage Method : GET");
		User user = new User();
		Group group = new Group();
		
		map.addAttribute("user", user);
	    map.addAttribute("group", group);
		return "/admin/users_manager/add_user";
	
	}
	
	@RequestMapping(value="/groups_manager", method=RequestMethod.GET)
	public ModelAndView groups_managerPage() {
		
		logger.info("groups_managerPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("admin/groups_manager");
		List<Group> groups = groupDAO.getAll();
		
		modelAndView.addObject("groups", groups);

		
		return modelAndView;
	
	}
	
	@RequestMapping(value="/groups_manager", method=RequestMethod.POST)
	public ModelAndView groups_managerPageEdit(ModelMap model,@ModelAttribute("Group")Group group,@RequestParam long group_id) {
	
		System.out.println("=====================here =================");
		System.out.println(group_id);
		System.out.println(group.getRoles().isEmpty());
		Set<Role> listRole2 =  new HashSet<Role>();
		Set<Role> listRole = group.getRoles();
		for (Role role : listRole) {
			System.out.println(role.getTitle());
			// listRole2.add(roleDAO.getById(Long.valueOf(role.getTitle())));
		}
		
		Group groupe = groupDAO.getById(group_id);
		//groupe.setRoles(listRole2);
		//groupDAO.add(groupe);
		ModelAndView modelAndView = new ModelAndView("admin/groups_manager");
		List<Group> groups = groupDAO.getAll();
		
		modelAndView.addObject("groups", groups);

		
		return modelAndView;
	
		//return "/admin/groups_manager";
		
		
	}
	
	@RequestMapping(value="/groups_manager/{id}")
	public ModelAndView roles_managerPage(@PathVariable("id") int id) {
		
		ModelAndView modelAndView = new ModelAndView("admin/roles_manager");

		
		
		Group groupnew = new Group();
		Group group =  groupDAO.getById(id);
		Set<Role> preCheckedVals= group.getRoles();
		groupnew.setRoles(preCheckedVals);
		
		modelAndView.addObject("group", groupnew);
		modelAndView.addObject("ide", id);
		List<Role> roles = roleDAO.getAll();
		modelAndView.addObject("roles", roles);
		return modelAndView;
		
		
		
		
		
		
		
		
		
		
		
	
	}


}
